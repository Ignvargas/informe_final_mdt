\chapter{Linux embebido}

En este capítulo se describe la creación de un sistema operativo Linux embebido personalizado, la selección y compilación del kernel y la construcción de un sistema de archivos utilizando la herramienta Buildroot.

\section{Descripción de un sistema Linux embebido}

De todos los sistemas operativos con compatibilidad para trabajar con la Zedboard, Linux se establece como la mejor opción ya que al ser un sistema open source cuenta con activas comunidades de desarrolladores las cuales comparten sus conocimientos y códigos fuentes. La idea de integrar un S.O. como Linux sobre el SoC Zynq es acercar el mundo de programación de alto nivel al desarrollo de núcleos hardware dedicados para sacar provecho de ambos mundos.

En la figura \ref{fig:LinuxEmb} se muestran los componentes básicos de una distribución Linux embebida. La imagen de arranque (\textit{boot.ini}) es un archivo binario que consiste en diferentes componentes que son necesarios para el arranque del S.O. El segundo se trata del archivo binario Device Tree (\textit{devicetree.dtb}), el cual contiene una estructura de datos que describe el hardware disponible, la imagen del kernel (\textit{uImage}) y el sistema de archivos root (\textit{uramdisk.image.gz}) que contiene las aplicaciones básicas del sistema operativo, todos cargados a memoria RAM por el U-Boot (del inglés Universal Bootloader) \cite{DevL}.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.4\textwidth]{Linux_emb}
	\caption{Componentes de una distribución linux embebida \cite{DevL}.}
	\label{fig:LinuxEmb}
\end{figure}

\subsection{Secuencia de arranque}

En la figura \ref{fig:BootL} se muestra la secuencia de pasos que se ejecuta cada vez que el sistema operativo se inicia. A continuación se detallan en qué consisten cada uno de ellos.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\textwidth]{Boot_linux}
	\caption{Secuencia de arranque de un sistema Linux embebido \cite{DevL}.}
	\label{fig:BootL}
\end{figure}

\subsubsection{Etapa 0}

El código de esta etapa se almacena en una BootRom interna. Su función es configurar el procesador ARM y los periféricos básicos, cargando sus drivers desde uno de los dispositivos de arranque, el cual reconoce luego de revisar la configuración de arranque dado por los pines MIO[5:3] (puede ser desde la memoria flash (NAND o NOR), SD o puertos QSPI JTAG), lugar desde el cual finalmente la BootRom cargar la \ac{FSBL} dentro de la memoria RAM.

\subsubsection{Etapa 1}

Esta etapa se carga el bitstream de la \ac{PL} por el cual se programa la lógica programable e inicializa el \ac{PS} utilizando los datos de configuración.

\subsubsection{Etapa 2}

En esta etapa se utiliza el \ac{U_Boot}, el cual es un cargador \acs{GLP}, cuya función es verificar e inicializar el hardware básico del sistema, carga el device tree en RAM como también la imagen descomprimida del kernel. Luego, entrega el control al kernel, junto con los parámetro de arranque (bootargs). Finalmente el kernel monta la imagen de los archivos del sistema iniciando los programas de inicio.

\section{Soporte HDMI para Zedboard sobre Linux}

Uno de los primeros desafíos fue encontrar la manera de utilizar la salida HDMI de la Zedboard desde Linux. La tarjeta trae incorporado un chip transmisor ADV7511, el cual se encarga de realizar el proceso de codificación y generación de señales del protocolo HDMI para la transmisión de video. El fabricante del chip transmisor ADV7511, Analog Devices, posee dentro de sus repositorios una referencia de diseño HDL para ser utilizado como proyecto base, el cual trae ya incorporado todos los módulos necesarios dentro de la lógica programable que permiten utilizar el transmisor ADV7511, además de un kernel de Linux con todos los drivers necesarios para utilizarlo.

\subsection{Diseño hardware base}

El diseño hardware de referencia \cite{refAnalog} posee 2 partes. La primera es la encargada de la transmisión de video. Como se aprecia en la fig. \ref{fig:hdl_zed_hdmi}, éste comienza con un IP Core VDMA desde el cual se accede al framebuffer en la memoria RAM (el cual es una región de memoria continua), que luego son transmitidos al módulo HDMI Core en el cual pasan por un proceso de conversión de espacios de colores de RGB a YCbCr y por un submuestreo de los datos de 444 a 422 siendo finalmente enviados al transmisor ADV7511. Por omisión, este último está configurado para funcionar bajo una resolución de  1080p, con un reloj de píxel de 148.5MHz, sin embargo es posible modificar accediendo a los registros internos de configuración del transmisor.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{hdl_hdmi_zed}
	\caption{Arquitectura para el soporte hdmi en linux.}
	\label{fig:hdl_zed_hdmi}
\end{figure}

La segunda parte se ocupa de la transmisión de audio. Referenciando nuevamente a la fig. \ref{fig:hdl_zed_hdmi}, esto se logra utilizando un IP core DMA el cual accede directamente a la memoria DDR del lado del PS, leyendo los datos de audio, siendo éstas palabras de 16 bits para cada uno de los canales (izquierdo y derecho), siendo finalmente enviados al módulo SPDIF encargado de transmitirlo al transmisor ADV7511.

Este diseño, se utilizará como base para las posteriores implementaciones, ya que posee todo lo necesario para el despliegue de datos por hdmi. En la fig. \ref{fig:recursos_hdmi}, se aprecia la tabla resumen de la utilización de recursos lógicos luego de realizar la implementación del diseño utilizando la herramienta Vivado 2015.2.1 (como anexo, se actualizó el diseño para que este funcione con herramientas más modernas, específicamente la versión 2016.1 de Vivado, corrigiendo los errores de compatibilidad generados por la actualización). Se puede notar que la implementación no utiliza más del 5\% de BRAM, DSP, Flipflops y LUT.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.7\textwidth]{recursos_hdmi}
	\caption{Resultados de la implementación en la FPGA para el soporte del HDMI.}
	\label{fig:recursos_hdmi}
\end{figure}

\section{Compilación kernel de linux}

Finalmente, se clonó directamente el Kernel de Linux del repositorio de Analog Devices \cite{kernelAnalog}, el cual trae ya instalados los drivers para el soporte de la arquitectura hardware para el HDMI antes descrita. Se procedió a configurarlo para arquitectura de la Zedboard, haciendo finalmente la compilación cruzada, utilizando el compilador ARM incluido en la herramienta SDK de Xilinx, generando una imagen del Kernel lista para utilizar.

\section{Compilación sistema operativo Linux}

\subsection{Root file system}

Una vez obtenido el kernel, es necesario buscar unos archivos de sistema raíz (del inglés, Root File System, abreviado rootfs) compatibles con éste. Existen varias versiones ya configuradas y listas para ser utilizadas juntas con algún kernel de Linux. Bajo esta perspectiva se evaluó el uso de dos, una llamada Linaro Ubuntu ARM y Petalinux provisto por Xilinx. Para cada una de estas se encontraron las siguientes desventajas.

La primera genera un sistema con un uso excesivo de recursos, utilizados en procesos que no son necesarios. Además hace uso de un servidor gráfico, lo cual es algo totalmente innecesario de implementar, ya que la biblioteca Qt5 permite generar interfaces gráficas directamente sobre un framebuffer.

En cuanto a la segunda opción, si bien es mucho más liviano y simple, se encontró con algunas restricciones al momento de personalizarlo. Si bien Xilinx, el proveedor de esta versión, entrega una herramienta para esta tarea, ella es muy limitada y no permite configurar algunos aspectos que se consideraron importantes.

Una vez evaluadas estas dos alternativas sin quedar conforme con ninguna de las dos, se optó por generar el rootfs de manera manual, utilizando herramientas especializadas para ello, como son Yocto y Buildroot. Ambas se caracterizan por optimizar el proceso de creación de un sistema de archivos raíz personalizado, permitiendo realizar todas las configuraciones mediante una única interfaz (gráfica o sobre una terminal) dejando que el usuario especifique qué es lo que desea instalar, para luego realizar la descarga de todos los archivos requeridos y compilarlos para el sistema objetivo. La diferencia entre ambos radica en el nivel de personalización al cual se puede llegar, lo cual tiene directa relación con la complejidad del diseño. Existen numerosos artículos dedicados a explicar la diferencia entre ambas herramientas, la cual en términos simples y sin entrar muy profundo en este tema, es que Buildroot, a diferencia de Yocto, realiza optimizaciones y oculta un porcentaje del proceso de configuración al usuario, impactando directamente sobre las cosas que se deseen hacer, sin embargo simplificando a la vez el proceso \cite{embSystemLinux} \cite{buildroot}. Se escogió Buildroot como herramienta para gestionar la creación de un sistema de archivos personalizado, con el objetivo de generar un sistema liviano, optimizado para utilizar la mínima cantidad de recursos.

\subsection{Configuración Buildroot}

A continuación se describen algunos de los paquetes y configuraciones utilizados en la compilación del rootfs.
Como arquitectura objetivo, se utilizó la ARM (Little endian), con soporte para el procesador cortex-A9 junto al módulo NEON como estrategia utilizada para la operación de punto flotantes. En cuanto a las opciones de configuración durante la compilación, se utilizó un nivel 3 de optimización, utilizando bibliotecas compartidas (shared libraries). Para la compilación cruzada, se utilizó un compilador ofrecido por buildroot (el incluido en la herramientas SDK de Xilinx que fue utilizado en la compilación del kernel, generaba ciertos problemas de compatibilidad), utilizando la librería glibc 2.23, incluyendo soporte para MMU. Como API gráfico se utilizó la biblioteca DirectFB que permite a las aplicaciones interactuar de manera directa con el framebuffer. Además se incluyó en el sistema el framework Qt5, que es utilizada para la creación de interfaces gráficas de usuario, el cual en esta versión en especial, puede funcionar sin la necesidad de un servidor gráfico, logrando correr directamente sobre el framebuffer. Finalmente se incluyó la librería OpenCV 2.4, considerada útil para futuras aplicaciones.

El sistema operativo compilado, ocupa un espacio en disco (en este caso la tarjeta SD) de 7.92MB, para la partición donde se encuentra el kernel y bootloader, y 235MB para la partición del rootfs, la cual se puede reducir aún más evitando compilar los programas de ejemplos para probar el framebuffer y la biblioteca Qt. En cuando al uso de recursos, el sistema hace uso de tan sólo 25 MB de RAM, con un 99\% del tiempo ocioso cuando se encuentra sin uso.

%Para probar su funcionamiento, se ejecutaron los ejemplos disponibles para el framebuffer y la biblioteca Qt. En la \ref{fig:fb_1} se despliega en el monitor un fondo estático, con una animación en el centro, la cual se muestra fluida y sin cortes, el de la \ref{fig:fb_2} se despliega una animación de múltiples Tux, que caminan por toda la pantalla, siendo una prueba de estrés del sistema, alcanzando 20 fps con 200 Tux.

%\begin{figure}[tb]
%	\centering
%	\includegraphics[width=0.7\textwidth]{fb_example_1}
%	\caption{Ejemplo funcionamiento DirectFB y salida HDMI.}
%	\label{fig:fb_1}
%\end{figure}

%\begin{figure}[tb]
%	\centering
%	\includegraphics[width=0.7\textwidth]{fb_example_2}
%	\caption{Ejemplo funcionamiento DirectFB, generando animaciones sin aceleración por hardware.}
%	\label{fig:fb_2}
%\end{figure}
