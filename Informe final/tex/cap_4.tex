\chapter{Cámaras infrarrojas y algoritmo NUC}

Como demostración del correcto funcionamiento del framework, se implementó un sistema de procesamiento de video infrarrojo con un algoritmo de corrección de no uniformidad basado en el trabajo desarrollado por ex-estudiantes del laboratorio de VLSI. En este capítulo busca introducir el problema de no uniformidad en las cámaras térmicas y revisar el algoritmo propuesto para su solución.

\section{Cámaras térmicas}

Una cámara infrarroja normal, esta compuesta por 3 subsistemas, como se puede ver en la figura \ref{fig:IR_cam_diag}. El sistema óptico se encarga de focalizar la radiación infrarroja hacia un arreglo de detectores, denominados Focal Plane Array (IRFPA), los cuales se encargan de convertir la energía óptica en señales eléctricas, las cuales son amplificadas electrónicamente adaptándolas para su visualización, tarea a cargo del ROIC.

\begin{figure}[tb]
\centering
	\includegraphics[width=0.6\textwidth]{IR_cam_diag}
	\caption{Diagrama de componentes de una cámara IR. \label{fig:IR_cam_diag}}
\end{figure}

Un microbolómetro, es una membrana sobre una estructura de soporte fabricado en tecnologías de Silicio Amorfo (A-Si) u Óxido de Vanadio (VOx), la cual se monta sobre el ROIC (circuito integrado de lectura). En esencia un microbolómetro consiste de un área sensible a la radiación sensible a la radiación infrarroja, la cual la convierte en calor, afectando su resistencia eléctrica. Los brazos, como se ve en la figura \ref{fig:diag_microbolometer}, permiten la conducción térmica, permitiendo que los cambios físicos debido a la incidencia de radiación sean procesados por la electrónica del ROIC, generando finalmente una imagen.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.4\textwidth]{diag_microbolometer}
	\caption{Diagrama de un microbolómetro. Cada microbolómetro corresponde a un pixel.}
	\label{fig:diag_microbolometer}
\end{figure}



\section{Corrección de no uniformidad}

Debido a imperfecciones de la elaboración y gradientes en el proceso de fabricación de los IRFPAs, la respuesta frente a él mismo estímulo infrarrojo de los fotodetectores no es homogénea, problema que se conoce como no uniformidad (NU). Esta se caracteriza por estar compuesta por un componente de ruido espacial u otro de ruido temporal.

Redlich \cite{Redlich} propone un algoritmo de NUC basado en escenas, el cual permite obtener como resultado una corrección visualmente correcta, pero con pérdidas en la capacidad de hacer mediciones radiométrica, es decir, el poder estimar la temperatura de manera precisa. Wolf \cite{Wolf} propone un modelo basado en una representación más fiel del comportamiento físico de un IRFPA, lo cual permite al algoritmo final propuesto realizar mediciones de temperatura utilizando las imágenes térmicas obtenidas de la corrección.

Su modelo propuesto se basa en la respuesta eléctrica de un arreglo de microbolómetros, el cual está dado definido en \ref{eq:regularmodel}, en el cual $V_{out}^{i,j}$ es el voltaje medido, $i_{bias}^{i,j}$ es la corriente de polarización del arreglo, $R^{i,j}$ la resistencia del microbolómetro sin flujo ""IR"" y $V_{elec}^{i,j}$ representa el ruido eléctrico o a elementos resistivos distintos a $R^{i,j}$ , todo estos definidos para cada uno de los pixeles ${i,j}$ del arreglo.

\begin{equation}
	V_{out}^{i,j} = i^{i,j}_{bias}   R^{i,j}+ V_{elec}^{i,j},\label{eq:regularmodel}
\end{equation}

Con base en esto, propone un modelo para determinar la resistencia $R^{i,j}$, mostrado en la ecuación \ref{eq:RBoloDependence2}, el cual considera el valor de este en un instante inicial, sumado a todos los parámetros que afectan su valor, lo cual se debe a la variación de su temperatura, tales como la emitancia IR medida $X^{i,j}[k]$, la emitancia IR generada por el propio armazón del dispositivo $X_h[k]$, cambios de temperatura debido a la disipación de potencia $\Delta T_{p}$ y a variación de la temperatura ambiente $\Delta T_{s}$.

\begin{equation}
	R^{i,j}[k] =  R_0^{i,j} + \Delta R^{i,j}_\phi (X^{i,j}[k]) + \Delta R_\phi (X_h[k])
		 +\Delta R^{i,j}(\Delta T_{p}[k]) + \Delta R^{i,j}(\Delta T_{s}[k]),
	\label{eq:RBoloDependence2}
\end{equation}

Luego, encuentra ecuaciones con base en parámetros físicos, que represente a cada uno de los elementos que afectan el valor de la resistencia, anteriormente descritos. Finalmente, luego de agrupar terminas y hacer suposiciones que simplifican ciertos parámetros, llega al modelo mostrado en la ecuación \ref{eq:NUCMODEL} que equivale a la emitancia IR de cada píxel, luego de la corrección del ruido de NU.

\begin{equation}
	X^{i,j}[k]= \frac{V_{out}[k]^{i,j}}{S_0^{i,j}} - S_1^{i,j} - (\Delta T_{p}[k] + \Delta T_{s}[k]) S_2^{i,j} - X_h[k],
	\label{eq:NUCMODEL}
\end{equation}

de la cual, los parámetros $S_k^{i,j}$ los define como: $S^{i,j}_0 = \frac{i^{i,j}_{bias} R^{i,j}_0 \alpha^{i,j}}{G^{i,j}\sqrt{1+\omega^2\tau_{th}^2}}$, $S^{i,j}_1 = \frac{G^{i,j}\sqrt{1+\omega^2\tau_{th}^2}}{\alpha^{i,j}}$ y $S^{i,j}_2 = G^{i,j}\sqrt{1+\omega^2\tau_{th}^2}$.

El problema con el modelo de la ecuación \ref{eq:NUCMODEL}, es que está definida por parámetros y variables de estado que no son tarea sencilla de determinar, es por eso que tomando este como base, comienza a hacer suposiciones y simplificaciones con tal de obtener un algoritmo en el cual los únicos componentes que varíen en el tiempo sean escalares u homogéneos, por lo que propone la ecuación \ref{eq:SNUC}.

\begin{equation}
	\widehat{X}^{i,j}[k]= \frac{V_{out}[k]^{i,j}}{S_0^{i,j}} - S_1^{i,j} - f(\Delta\overline{T_1})[k] S_2^{i,j} - \Omega(\overline{T_2})[k],
	\label{eq:SNUC}
\end{equation}

En donde $f(\Delta\overline{T_1})[k]$ corresponda a una función que estima variaciones en la temperatura del arreglo, con $\Delta\overline{T_1}$  equivalente a la variación de temperatura del \ac{FPA} con respecto a una temperatura inicial.
Por otro lado $\Omega(\overline{T})[k]$ modela el comportamiento de $X_h[k]$, compensa errores y variaciones uniformes, con $\overline{T_2}$ la medición de temperatura del sensor integrado en el núcleo. Si $\Omega(\overline{T})[k]$ se ignora, el algoritmo funciona, pero solo realizaría una corrección visual.

Utilizando la calibración de cuerpos negros, a dos temperaturas distintas, manteniendo $T_{FPA\_0}$ lo más estable posible, los términos $\widehat{S}_0^{i,j}$ y $\widehat{S}_1^{i,j}$ se pueden determinar por las ecuaciones \ref{eq:Aprima} y \ref{eq:S_1_shape}, donde $Y_1$ y $Y_2$ son las imágenes sin corregir y $T_{bb 1}$ y $T_{bb 2}$ las temperaturas de ambos cuerpos negros.

\begin{equation}
	\widehat{S}_0^{i,j} = \frac{Moda( Y_1 - Y_2)}{Y_1^{i,j} - Y_2^{i,j}}
	\label{eq:Aprima}
\end{equation}

Se utiliza la moda de ($Y_1-Y_2$), (en lugar de $T_{bb 1}-T_{bb 2}$) ya que evita que el resultado de la operación a nivel de bits cambie de resolución. Esto provoca un error en $\widehat{S}_0^{i,j}$, lo cual se compensa agregando una constante de proporcionalidad $f_0[k]$.

\begin{equation}
	  \widehat{S}_1^{i,j}= \frac{Y_1^{i,j}}{S_0^{i,j}} - T_{bb 1}.
	\label{eq:S_1_shape}
\end{equation}

Luego, realizando una tercera medición a un cuerpo negro, a dos temperaturas , pero manteniendo $T_{bb 1}$, $\widehat{S}_2^{i,j}$ se puede calcular como:

\begin{equation}
	\widehat{S}_2^{i,j} = \frac{Y_1^{i,j} - Y_3^{i,j}}{\widehat{S}_0^{i,j}\cdot (f(\overline{T}_{FPA\_0}) - f(\overline{T}_{FPA\_1}))}, %\propto S_2^{i,j},
	\label{eq:S_2_shape}
\end{equation}

De esta última, el calculo se puede simplificar, haciendo los términos $f(\Delta\overline{T}_{FPA\_0})=0$ y $f(\Delta\overline{T}_{FPA\_1})=\overline{T}_{FPA\_1} - \overline{T}_{FPA\_0}$, lo cual produce un error pero es absorbido por el término $f(\Delta\overline{T_1})[k]$.

\subsection{Estimación de valores de $f_0$, $f(\Delta\overline{T_1})$ y $\Omega(\overline{T})$}

Las expresiones $f_0$ y $u=f(\Delta\overline{T_1})$ se pueden considerar constantes, ya que son variables de dinámica lenta y se estiman utilizando una función de costo que refleja la diferencia entre el valor esperado y el actual de un pixel. El valor esperado se calcula usando la ecuación \ref{eq:expected_mio}, utilizando una ventana de $W=1$.

\begin{equation}
	 E(\widehat{X}^{i,j}[k]) = \frac{1}{(2W + 1)^2}\sum_{m=i-W}^{m=i+W} \sum_{n=j-W}^{n=j+W} \widehat{X}^{n,m}[k],
	\label{eq:expected_mio}
\end{equation}

La función de costos se define como:

\begin{subequations}
	\begin{align}
	 \text{CF}[k]  &= \sum^{i,j}{ \left(\widehat{X}^{i,j}[k] - E(\widehat{X}^{i,j}[k])\right)^2},\\
	\frac{\delta \text{CF}}{ \delta f_0}[k] &= 2 \sum^{i,j}{\left(\left(\widehat{X}^{i,j}[k] - E(\widehat{X}^{i,j}[k])\right) \cdot \widehat{S}_1^{i,j}\right)}\label{eq:deltaCF1},\\
	\frac{\delta \text{CF}}{ \delta u}[k] &= 2 \sum^{i,j}{\left(\left(\widehat{X}^{i,j}[k] - E(\widehat{X}^{i,j}[k])\right) \cdot \widehat{S}_2^{i,j}\right)}\label{eq:deltaCF2}.
	\end{align}
\end{subequations}

Finalmente, los valores de $u[k]$ y $f_0[k]$, se determinan utilizando las ecuaciones \ref{eq:deltaCF1} y \ref{eq:deltaCF2} anteriores en las ecuaciones \ref{eq:shape1} y \ref{eq:shape2}, utilizando tasas de aprendizaje $\eta_1=0.0012$ y $\eta_2=6.1035\times10^{-12}$

\begin{subequations}
	\begin{align}
		u[k]  &= u[k-1] -\eta_1\cdot\frac{\delta CF}{ \delta u}[k-1],\label{eq:shape1}\\
		f_0[k]  &= f_0[k-1] -\eta_2\cdot\frac{\delta CF}{ \delta f_0}[k-1],\label{eq:shape2}
	\end{align}
\end{subequations}

Finalmente, el modelo propuesto que corrige el NUC está dado por la ecuación \ref{eq:SNUC_learn}.

\begin{equation}
	\widehat{X}^{i,j}[k] = \frac{Y^{i,j}[k]}{S_0^{i,j}} + \widehat{f}_0[k]\cdot \widehat{S}_1^{i,j} + u[k]\cdot \widehat{S}_2^{i,j} + \widehat{\Omega}(T_s)[k],
	\label{eq:SNUC_learn}
\end{equation}
