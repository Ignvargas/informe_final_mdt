\chapter{Implementación sistema procesamiento de video infrarrojo}

En este capítulo se aborda la adaptación del diseño de corrección de no uniformidad desarrollado por Wolf \cite{Wolf}, del cual se extrajeron los núcleos de procesamiento que implementan los algoritmos de corrección con el objetivo de integrarlos en la nueva arquitectura, para lo cual se diseñaron nuevas lógicas de control basada en las interfaces Axi-S y Axi-Lite. Además se integró soporte en el driver diseñado para el control de estos módulos desde una aplicación en Linux.
Se realizó todo esto con el objetivo de demostrar el correcto funcionamiento del framework y además entregar un diseño que pueda ser usado como referencia para futuras implementaciones.

Finalmente, se explica el funcionamiento de la interfaz Qt diseñada que despliega los resultados en una interfaz gráfica con la que el usuario final puede interactuar directamente sobre los núcleos de procesamiento en hardware junto con el rendimiento general del sistema.


\section{Procesamiento de video infrarrojo}

\subsection{El problema de no uniformidad en cámaras térmicas}

Una cámara infrarroja normal, está compuesta por 3 subsistemas, como se puede ver en la figura \ref{fig:IR_cam_diag}. El sistema óptico se encarga de focalizar la radiación infrarroja hacia un arreglo de detectores, denominados Focal Plane Array (IRFPA), los cuales se encargan de convertir la energía óptica en señales eléctricas, las cuales son amplificadas electrónicamente adaptándolas para su visualización, tarea a cargo del ROIC.

\begin{figure}[tb]
\centering
	\includegraphics[width=0.6\textwidth]{IR_cam_diag}
	\caption{Diagrama de componentes de una cámara IR. \label{fig:IR_cam_diag}}
\end{figure}

Un microbolómetro, es una membrana sobre una estructura de soporte fabricado en tecnologías de Silicio Amorfo (A-Si) u Óxido de Vanadio (VOx), la cual se monta sobre el ROIC (circuito integrado de lectura). En esencia un microbolómetro consiste de un área sensible a la radiación sensible a la radiación infrarroja, la cual la convierte en calor, afectando su resistencia eléctrica. Los brazos, como se ve en la figura \ref{fig:diag_microbolometer}, permiten la conducción térmica, permitiendo que los cambios físicos debido a la incidencia de radiación sean procesados por la electrónica del ROIC, generando finalmente una imagen.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.4\textwidth]{diag_microbolometer}
	\caption{Diagrama de un microbolómetro. Cada microbolómetro corresponde a un pixel.}
	\label{fig:diag_microbolometer}
\end{figure}

Debido a imperfecciones de la elaboración y gradientes en el proceso de fabricación de los IRFPAs, la respuesta frente a él mismo estímulo infrarrojo de los fotodetectores no es homogénea, lo que provoca lecturas erróneas que deben ser corregidas. Este problema se conoce como no uniformidad (NU).

\subsection{Adquisición de video desde la cámara infrarroja}

Como fuente de video, se utilizó una cámara infrarroja Tau conectada al puerto FMC de la tarjeta de desarrollo utilizando el protocolo de comunicación \textit{Camera Link} por el cual se transmite el video a una tasa de 30 FPS. Para lograr procesar el video es necesario convertir de Camera Link al protocolo AXIS ya que este es el que se definió como estándar de transmisión de video entre los núcleos IP, además, permite trabajar con flujo de datos más compactos y homogéneos utilizando el reloj del AXIS.
En la fig. \ref{fig:cl_axis} se muestra el diagrama simplificado del hardware diseñado para esta tarea. El núcleo IP diseñado, recibe los datos por Camera Link y los guarda temporalmente dentro de una memoria FIFO hasta que lleguen todos los pixeles de una línea de video, momento en el cual estos son transmitidos a toda velocidad por el puerto AXIS maestro.

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.8\textwidth]{cl_axis}
 \caption{Arquitectura módulo Camera Link a AXIS.}
 \label{fig:cl_axis}
\end{figure}

\subsection{Corrección de no uniformidad}

El modelo propuesto por Wolf \cite{Wolf} que corrige el problema de no uniformidad está dado por la ecuación \ref{eq:SNUC_learn}.

\begin{equation}
	\widehat{X}^{i,j}[k] = \frac{Y^{i,j}[k]}{\widehat{S}_0^{i,j}} + \sum_{p=1}^{p=6}{(\widehat{f}_p[k] \cdot \widehat{S}_p^{i,j})}
	\label{eq:SNUC_learn}
\end{equation}

Donde $\widehat{S}_p^{i,j}$ y $\widehat{S}_0^{i,j}$ corresponden a patrones obtenidos como resultado de la calilbración de 2 puntos. Este modelo está basado en 3 temperaturas de calibración.

Los valores de cada $f_p[k]$, se determina utilizando la ecuación \ref{eq:shape2}, utilizando tasas de aprendizaje $\eta_1=0.0012$ y $\eta_2=6.1035\times10^{-12}$.

\begin{equation}
		\widehat{f}_p[k] = \widehat{f}_p[k] -\eta_p\cdot\frac{\delta CF}{ \delta f_p}[k-1],
		\label{eq:shape2}
\end{equation}

La función de costos se define en \ref{eq:deltaCF}.

\begin{equation}
	\frac{\delta \text{CF}}{ \delta \text{f}_p}[k] = 2 \sum^{i,j}{\left(\left(\widehat{X}^{i,j}[k] - E(\widehat{X}^{i,j}[k])\right) \cdot \widehat{S}_p^{i,j}\right)}
	\label{eq:deltaCF},
\end{equation}

Finalmente, el valor esperado se obtiene de \ref{eq:expected_mio}, utilizando una ventana de $W=1$.

\begin{equation}
	 E(\widehat{X}^{i,j}[k]) = \frac{1}{(2W + 1)^2}\sum_{m=i-W}^{m=i+W} \sum_{n=j-W}^{n=j+W} \widehat{X}^{n,m}[k],
	\label{eq:expected_mio}
\end{equation}

\subsection{Rediseño de arquitectura NUC}

En la fig. \ref{fig:arqui_nuc_old} se muestra un esquema general del funcionamiento de la arquitectura hardware diseñada originalmente por Wolf para realizar la corrección de no uniformidad. El módulo central, llamado \textit{Compensador}, es el que implementa las ecuaciones del algoritmo de corrección. Este se alimenta de múltiples fuentes de datos, el frame anterior (PastFrame), los patrones de corrección (pattern 1, 2 y 3) y el frame actual, los cuales son traídos desde la memoria RAM externa. Los registros de control del módulo son manipulados por interruptores (SW) físicos de la tarjeta.

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.6\textwidth]{arqui_nuc_old}
 \caption{Arquitectura simplificada original del módulo NUC.}
 \label{fig:arqui_nuc_old}
\end{figure}

La adquisición y envío de datos es realizado por módulos externos que interactúan con la memoria RAM, los cuales utilizan un offset, no modificable por software, de la posición en memoria a utilizar. Cada nueva transacción tanto de lectura como de escritura son realizadas cuando recibe una señal de activación. Tanto los datos obtenidos como los que se desean enviar son guardados temporalmente dentro una BRAM.

El bloque \textit{Tau Data} es el encargado de recibir los datos de la cámara infrarroja conectada a los puertos GPIO de la tarjeta utilizando el formato LVDS, por lo que antes de ser utilizadas pasa por un proceso de decodificadas y deserialización antes de alimentar al siguiente módulo.

La adaptación del módulo compensador se muestra en la \ref{fig:arqui_nuc}. Lo primero que destaca es el reemplazo de las BRAM por puertos AXI Stream, los cuales se comunican con los módulos VDMA que internamente tienen incorporado buffers de línea para almacenar los datos temporalmente. El módulo \textit{CL to AXIS} es el descrito anteriormente. Los valores de las constantes de aprendizaje y los bits de control del módulo Compensador está controlados por registros internos que son mapeados a través del protocolo AXI-Lite al sistema, logrando que estos puedan ser vistos y configurados desde Linux.

 \begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\textwidth]{arqui_nuc}
	\caption{Arquitectura modificada del módulo NUC.}
	\label{fig:arqui_nuc}
\end{figure}

En la tabla \ref{tab:reg_nuc} se muestran todos los registros internos del módulo que son configurables por software.

\begin{table}[ht]
\begin{center}
\caption{Registros internos módulo NUC}
\begin{tabular}{| l | c | p{7cm} |}
	\hline
    \textbf{Offset} & \textbf{Nombre} & \textbf{Descripción} \\ \hline
    0x0 & Tasa de aprendizaje 1 & $\eta_1$ patrón 1 \\ \hline
    0x4 & Tasa de aprendizaje 2 & $\eta_2$ patrón 1\\ \hline
    0x8 & Tasa de aprendizaje 3 & $\eta_1$ patrón 2 \\ \hline
    0x12 & Tasa de aprendizaje 4 & $\eta_2$ patrón 2\\ \hline
    0x16 & Tasa de aprendizaje 5 & $\eta_1$ patrón 3 \\ \hline
    0x20 & Tasa de aprendizaje 6 & $\eta_2$ patrón 3 \\ \hline
    0x24 & Control & Configura y controla módulo NUC.\\ \hline
\end{tabular}
\label{tab:reg_nuc}
\end{center}
\end{table}

Los registros de la las tasas de aprendizajes, como se ve en la fig. \ref{fig:reg_nuc1} son valores positivos de 32 bits. En la fig. \ref{fig:reg_nuc2} se muestra la estructura del registro de control, el bit RstParams resetea los parámetros de corrección, Snoise y Stripping son filtros incorporados mientras que NUCLearn inicia el entrenamiento del algoritmo, todos activados con el valor \textit{1}. El bit 0 RstSync permite reiniciar la sincronización de todos los datos de entrada en caso de que por algún motivo estos no se sincronicen de manera correcta.

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.7\textwidth]{reg_nuc1}
 \caption{Registros tasas de aprendizaje.}
 \label{fig:reg_nuc1}
\end{figure}

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.7\textwidth]{reg_nuc2}
 \caption{Registro control módulo NUC.}
 \label{fig:reg_nuc2}
\end{figure}

\subsection{Soporte al módulo NUC desde el driver.}

Para controlar el módulo IP NUC desde una aplicación en el espacio del usuario se agregaron las siguientes llamadas ioctl.

\textbf{CMD: CONFIG\_NUC}

Este comando recibe como argumento un puntero a la estructura \textbf{config\_NUC} \ref{tab:struct_config_NUC}. Internamente mapea los registros con la función \ref{eq:ioremap} y envía las tasas de aprendizaje de la subestructura \ref{tab:struct_eta_values} y los parámetros de configuración de la estructura \ref{tab:struct_set_NUC} haciendo los arreglos de bits para cumplir con la especificación de los registros de la fig. \ref{fig:reg_nuc2}.

\begin{table}[ht]
\begin{center}
\caption{Estructura \textbf{config\_NUC}}
\begin{tabular}{| l | c | p{7cm} |}
	\hline
	\textbf{Tipo de dato} & \textbf{Nombre variable} & \textbf{Uso} \\ \hline
	unsigned int & phys\_addr & Dirección física de los registros del IP. \\ \hline
	struct eta\_values & learn\_rate & Tasas de aprendizajes. \\ \hline
  	struct set\_NUC & NUC\_init & Configuración inicial. \\ \hline
\end{tabular}
\label{tab:struct_config_NUC}
\end{center}
\end{table}

\begin{table}[ht]
\begin{center}
\caption{Estructura \textbf{eta\_values}}
\begin{tabular}{| l | c | p{5cm} |}
	\hline
	\textbf{Tipo de dato} & \textbf{Nombre variable} & \textbf{Uso} \\ \hline
	unsigned int & Eta\_1 & Tasa de aprendizaje $\eta_1$ patrón 1. \\ \hline
  unsigned int & Eta\_2 & Tasa de aprendizaje $\eta_2$ patrón 1. \\ \hline
  unsigned int & Eta\_3 & Tasa de aprendizaje $\eta_1$ patrón 2. \\ \hline
  unsigned int & Eta\_4 & Tasa de aprendizaje $\eta_2$ patrón 2. \\ \hline
  unsigned int & Eta\_5 & Tasa de aprendizaje $\eta_1$ patrón 3. \\ \hline
  unsigned int & Eta\_6 & Tasa de aprendizaje $\eta_2$ patrón 3. \\ \hline
\end{tabular}
\label{tab:struct_eta_values}
\end{center}
\end{table}

\begin{table}[ht]
\begin{center}
\caption{Estructura \textbf{set\_NUC}}
\begin{tabular}{| l | c | p{8cm} |}
	\hline
	\textbf{Tipo de dato} & \textbf{Nombre variable} & \textbf{Uso} \\ \hline
	unsigned char & Rst\_params & Reinicia los parámetros de corrección. [Enable, Disable] \\ \hline
	unsigned char & Snoise & Estado del filtro Snoise. [Enable, Disable] \\ \hline
  unsigned char & Stripping & Estado filtro Stripping. [Enable, Disable] \\ \hline
  unsigned char & NUC\_learn & Estado entrenamiento del algoritmo. [Enable, Disable] \\ \hline
  unsigned char & Rst\_sync & Reinicia la sincronización con la entrada de video del módulo. [Enable, Disable] \\ \hline
\end{tabular}
\label{tab:struct_set_NUC}
\end{center}
\end{table}

\textbf{CMD: CHANGE\_NUC}

Este comando recibe como argumento un puntero a la estructura \textbf{set\_NUC} \ref{tab:struct_set_NUC} con los cuales vuelve a escribir en el registro \ref{fig:reg_nuc2} actualizandolos con la nueva configuración.

\subsection{Rediseño módulo Colormap}

El módulo IP Colormap recibe la señal ya sea procesada o directo desde la cámara, escala los pixeles a un rango para controlar el contraste y les aplica un mapa de color, transformando los datos de 13 bits por pixel a 24 bits en formato RGB.
En \ref{fig:colormap_old} se describe la arquitectura original del mapa de colores que es aplicado a la imagen infrarroja. En el diseño original este recibe los datos desde el mismo módulo de lectura/escritura utilizado en la arquitectura del NUC. Una vez que se almacena una línea de video en la BRAM, esta es enviada al módulo \textit{imagesc}, que escala el valor de los datos crudos, dentro de un máximo y mínimo asignado por el usuario o buscado en línea para el caso de auto contraste, luego los datos son enviados al módulo colormap que realiza la conversión al formato RGB de colores.

 \begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{old_colormap}
	\caption{Arquitectura original del módulo Colormap.}
	\label{fig:colormap_old}
\end{figure}

En \ref{fig:colormap} se muestra la arquitectura propuesta, la cual reemplaza los módulos de adquisición de datos, por interfaces AXI Stream, que se comunican con módulos VDMA o por algún otro núcleo IP que utilice el protocolo. Además tanto la lógica de control como los registros internos fueron adaptados para trabajar con el nuevo protocolo.

 \begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{colormap}
	\caption{Arquitectura modificada del módulo Colormap.}
	\label{fig:colormap}
\end{figure}

En la tabla \ref{tab:reg_colormap}  se muestran los registros internos del IP Colormap.

\begin{table}[ht]
\begin{center}
\caption{Registros internos módulo colormap}
\begin{tabular}{| l | l | p{8cm} |}
    \hline
    \textbf{Offset} & \textbf{Nombre} & \textbf{Descripción} \\ \hline
    0x0 & Control & Configura y controla el funcionamiento del módulo Colormap. \\ \hline
    0x4 & Range Contrast & En contraste manual, configura el valor máximo y mínimo.\\ \hline
\end{tabular}
\label{tab:reg_colormap}
\end{center}
\end{table}

Del registro de control, mostrado en la fig. \ref{fig:reg_colormap1}, se utilizan los primeros 3 bits. MODE indica si el módulo funciona con auto contraste (valor '0') o con el rango configurado con el rango dado en el segundo registro, fig. \ref{fig:reg_colormap2}. El bit COLOR, cambia entre los distintos mapas disponibles ( entre el par gray, copper o gray, thermal), mientras que el bit VFilter, activa ('1') o deshabilita ('0') el filtro vertical.

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.7\textwidth]{reg_colormap}
 \caption{Registro control módulo Colormap.}
 \label{fig:reg_colormap1}
\end{figure}


\begin{figure}[ht]
 \centering
 \includegraphics[width=0.7\textwidth]{reg_colormap2}
 \caption{Registro configuración contraste manual.}
 \label{fig:reg_colormap2}
\end{figure}

\subsection{Soporte al módulo Colormap desde el driver.}

Para controlar el módulo IP Colormap desde una aplicación en el espacio del usuario se agregaron las siguientes llamadas ioctl.

\textbf{CMD: SET\_COLORMAP}

Este comando recibe como argumento un puntero a la estructura \textbf{config\_colormap} \ref{tab:struct_config_colormap}. Internamente mapea los registros con la función \ref{eq:ioremap} y envía los parámetros de configuración de la estructura \ref{tab:struct_set_colormap} haciendo los arreglos de bits para cumplir con la especificación de los registros de la fig. \ref{fig:reg_colormap2}


\begin{table}[ht]
\begin{center}
\caption{Estructura \textbf{config\_colormap}}
\begin{tabular}{| l | l | p{7cm} |}
    \hline
	\textbf{Tipo de dato} & \textbf{Nombre variable} & \textbf{Uso} \\ \hline
	unsigned int & phys\_addr & Dirección física de los registros del IP. \\ \hline
	struct set\_colormap & colormap & Configuración inicial del módulo. \\ \hline
\end{tabular}
\label{tab:struct_config_colormap}
\end{center}
\end{table}


\begin{table}[ht]
\begin{center}
\caption{Estructura \textbf{set\_colormap}}
\begin{tabular}{| l | c | p{8cm} |}
	\hline
	\textbf{Tipo de dato} & \textbf{Nombre variable} & \textbf{Uso} \\ \hline
	unsigned char & MODE & Contraste manual (\textbf{MANUAL\_MAP}) o automático (\textbf{AUTO\_MAP}). \\ \hline
	unsigned char & COLOR & Mapa de colores utilizado. Valor 1 para Cooper y 0 para Gray. \\ \hline
  unsigned char & V\_filter & Estado filtro vertical (\textbf{ON\_FILTER} o \textbf{OFF\_FILTER}) \\ \hline
  struct range\_colormap & range\_contrast & Rango del contraste manual.\\ \hline
\end{tabular}
\label{tab:struct_set_colormap}
\end{center}
\end{table}

\begin{table}[ht]
\begin{center}
\caption{Estructura \textbf{range\_colormap}}
\begin{tabular}{| l | c | p{5cm} |}
	\hline
	\textbf{Tipo de dato} & \textbf{Nombre variable} & \textbf{Uso} \\ \hline
	unsigned int & max & Valor máximo del contraste. \\ \hline
	unsigned int & min & Valor mínimo del contraste.\\ \hline
\end{tabular}
\label{tab:struct_range_colormap}
\end{center}
\end{table}

\textbf{CMD: SET\_COLORMAP}

Este comando recibe como argumento un puntero a la estructura \textbf{set\_colormap} \ref{tab:struct_set_colormap} y utiliza la misma función utilizada en el caso anterior, pero esta vez para actualizar los registros con la nueva configuración

\section{Nueva arquitectura completa}

El esquema del diseño hardware final utilizado para la corrección de no uniformidad se encuentra en \ref{fig:arqui_full}.

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.9\textwidth]{arqui_full}
 \caption{Arquitectura implementada para NUC.}
 \label{fig:arqui_full}
\end{figure}

Cada uno de los bloques IP VDMA, NUC y Colormap son configurados y controlados por software utilizando el driver como intermediario. Los tres VDMA relacionados con los patrones, utilizan solo el canal de lectura MM2S los cuales están configurados para utilizar un solo buffer, el que se lee de manera circular.
El \textit{VDMA\_PastFrame} utiliza ambos canales S2MM y MM2S (escritura y lectura) y están asociados al mismo banco de buffers. Estos están configurados en modo GenlockDynamic, lo que asegura que ambos canales lean y escriban al mismo tiempo, pero en buffers distinto, nunca accediendo al mismo.
Los \textit{VDMA\_Raw} y \textit{VDMA\_NUC} escriben en bancos distintos, de 2 buffers cada uno, los cuales son posteriormente mapeados al espacio del usuario para ser mostrados en la interfaz gráfica.

En la tabla \ref{tab:resource_nuc} se muestra un resumen de los recursos lógicos utilizados en la implementación del sistema NUC completo. En la columna \textit{Estimación NUC}, se calculó una aproximación de los recursos utilizados solo por los IP que realizan la corrección de no uniformidad, restando al total lo utilizado por la implementación base de la plataforma descrita en el capítulo 2.

\begin{table}[ht]
\begin{center}
\caption{Resultado implementación sistema NUC completo}
\begin{tabular}{| l | c | c | c |}
	\hline
	\textbf{Recurso} & \textbf{Utilización total} & \textbf{Utilización total (\%)} & \textbf{Estimación NUC (\%)} \\ \hline
	LUT & 17739 & 33.34 & 18.53 \\ \hline
	LUTRAM & 1156 & 6.64 & 3.93 \\ \hline
	Flip Flop & 22129 & 20.8 & 12.09 \\ \hline
	BRAM & 21.50 & 15.36 & 11.07 \\ \hline
	DSP & 57 & 25.91 & 19.09 \\ \hline
	IO & 85 & 42.50 & 9 \\ \hline
	BUFG & 7 & 21.88 & 3.13 \\ \hline
	MMCM & 2 & 50.0 & 0 \\ \hline
\end{tabular}
\label{tab:resource_nuc}
\end{center}
\end{table}

\section{Diseño interfaz Qt}

Una interfaz Qt normalmente está compuesta por varios elementos llamados widgets, los cuales son objetos que poseen una forma característica (botones, menús desplegables, cuadros seleccionables, etc.) y son desplegados en pantalla, los cuales esperan la interacción con el usuario o la ocurrencia de algún evento. Cuando este ocurre, internamente se ejecuta la rutina asociada al widget y se realiza algún tipo de acción. En este caso en particular, se utilizaron botones, botones de selección y cuadros de adquisición de valores numéricos.
En la fig. \ref{fig:gui} se muestra la interfaz gráfica diseñada. El botón \textit{Start Video} configura todas las estructuras de los VDMA utilizados y de los núcleos IP a utilizar, en este caso NUC y Colormap con la configuración inicial. Luego, utilizando las llamadas \textit{ioctl} se las envía al driver para que éste proceda a configurar los registros físicos de los módulos en el área PL utilizando los comandos que fueron definidos y documentados en este y en los capítulos anteriores.
Cada cuadrado negro es una salida del video procesado en el área PL, el primero corresponde a la salida corregida y el segundo a video sin procesar. Debajo de estos, se encuentran una serie de widgets, los cuales permiten interactuar con los núcleos IP NUC y Colormap. Cada vez que se interactúa con uno de ellos, internamente Qt ejecuta una rutina en donde se modifica el valor de la estructura correspondiente al módulo y se la envía al driver el cual se encarga de actualizar los registros físicos de estos con tal de actualizar sus parámetros de control.

\begin{figure}[tb]
 \centering
 \includegraphics[width=1\textwidth]{screen_app_qt}
 \caption{Interfaz gráfica ejecutando sobre Linux en la Zedboard.}
 \label{fig:gui}
\end{figure}

Cada uno de los cuadrados negros corresponde a un widget personalizado para el despliegue de video desde los bancos en memoria de los VDMA. Cada uno está relacionado con la ejecución de una hebra independiente que se encarga de actualizar los cuadros de video, en el la fig. \ref{fig:thread_qt} se muestra un diagrama de flujo de las tareas que esta realiza. Resumiendo, éstas reservan una región de memoria para las imágenes, donde la parte superior se agrega un \textit{header} correspondiente al formato \textit{PPM} (del inglés Portable Pixel Map) el cual permite trabajar con datos de pixeles crudos sin comprimir. El proceso de dar formato a la imagen que proviene desde Hardware es esencial, ya que éstos son usados para crear un objeto QImage, el cual sólo es compatible con ciertos formatos. El objeto QImage luego es enviado hacia el widget personalizado, el cual lo recibe y lo despliega en pantalla en la región correspondiente, luego, la hebra se duerme un tiempo \textit{Tsleep} que corresponde al frame rate que se desea desplegar el video. Cuando despierta, cambia al siguiente buffer en memoria del banco correspondiente al VDMA y vuelve a repetir el proceso.

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.3\textwidth]{thread_qt_video}
 \caption{Hebra de la aplicación Qt para el despliegue de video.}
 \label{fig:thread_qt}
\end{figure}


\section{Resultados de implementación}

En \ref{fig:setup_gui} se muestra el setup utilizado para la prueba del hardware y software diseñado que implementa la corrección de no uniformidad. La Zedboard, está conectado por el puerto FMC a la cámara infrarroja. En el puerto USB OTG se conectaron un mouse y teclado para interactuar con la aplicación.

 \begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{setup_gui}
	\caption{Setup experimental}
	\label{fig:setup_gui}
\end{figure}

Tanto el driver como la aplicación cumplen con sus funciones sin problemas, sin embargo existe un detalle en la manera en la cual se muestra video a través de la interfaz, el cual presenta un problema evidente cuando se intenta mostrar dos cuadros de video a la vez. La razón de esto, es que el método seleccionado realiza al menos dos copia a diferentes regiones de memoria antes de que el frame sea enviado al framebuffer. La primera ocurre cuando se crea un objeto QImage, como esta clase sólo trabaja bajo ciertos formatos de imágenes, es necesario agregar una cabecera al frame de video que llega desde el hardware para que este la reconozca como válido. La segunda es cuando se envía el objeto al widget, el cual Qt internamente vuelve a copiar la imagen a estructuras internas que se encargan de manejar el framebuffer. Toda la copia de los datos lo gestiona el procesador, al punto de llegar al 100\% de uso al intentar mostrar dos cuadros de video a 30fps, momento en el cual Linux cierra la aplicación por secuestrar el uso de los procesadores. En la tabla \ref{tab:recursos_cpu} se muestra una comparativa del uso de CPU al intentar mostrar 2 fuentes de video.

\begin{table}[ht]
\begin{center}
\caption{Resumen uso CPU utilizado por la aplicación Qt}
\begin{tabular}{| c | p{8cm} |}
	\hline
    \textbf{Uso de CPU} & \textbf{Caso} \\ \hline
    1\% & Sin mostrar video. \\ \hline
    35\% & Una fuente de video a 30fps. \\ \hline
    75\% & Dos fuentes de video a 15fps cada una. \\ \hline
    100\% & Dos fuente de video a 30fps cada una. \\ \hline
\end{tabular}
\label{tab:recursos_cpu}
\end{center}
\end{table}

En consecuencia, la aplicación está configurada para mostrar 1 fuente de video a 30 FPS, o dos a 15 FPS. La pérdida de rendimiento era esperable ya que el SoC Zynq no incorpora un módulo acelerador de video en el área PS, sin embargo no era posible estimar la real carga al CPU que involucraba esta tarea. Para este problema se plantean dos posibles soluciones. La primera es utilizar el mismo método descrito, pero modificando el código del widget base de Qt para que utilice el controlador DMA para que gestione la copia de datos, de tal manera de liberar al CPU de esta tarea.
La segunda opción, es solucionar el problema desde hardware escribiendo los cuadros de video directamente sobre la región perteneciente a los widgets en el framebuffer. Para esto, primero se debería verificar la distribución de los pixeles en el área de memoria del Framebuffer para que coincida con el formato enviado desde hardware, ya que el control del driver lo posee la aplicación de Qt, y es posible que éste la cambie por motivos de optimización o funcionamiento de la librería. Segundo, se debería editar la estructura de configuración de los VDMA del driver, agregando miembros que permitan identificar cuando se desee escribir directamente sobre el framebuffer y permite enviar una dirección física de memoria desde el espacio del usuario para que sea enviada al módulo VDMA.
