 ***** Write channel *****

Genlock Master.
    - When configured as Genlock Master, the channel does not skip or
    repeat the frames. Genlock Slave should follow the Genlock Master 
    with a predetermined frame delay value set in its.
 
 
 Dynamic Genlock Master

    -   When configured as Dynamic Genlock Master, the channel skips
    the frame buffers that Dynamic Genlock Slave is operating on. It 
    is done by either skipping or repeating frames. 

    To set up the channel in Dynamic Genlock Master mode, the following settings should be
    used:
    - Set GenlockEn (S2MM_VDMACR[3]=1) to enable Genlock synchronization between
    master and slave.
    - Set GenlockSrc (S2MM_VDMACR[7]=1) to enable Internal Genlock mode. This bit is set
    to 1 by default if both channels are enabled in the Vivado IDE. When it is set to 1, you
    do not need to connect *_frame_ptr_out and *_frame_ptr_in signals externally.
    They are routed internally in the core.

**** Read channel *****

Genlock Slave

    -    When configured as Genlock Slave, the channel tries to catch up
    with the Genlock Master either by skipping or repeating frames.
     It samples the Genlock Master frame number on mm2s_frame_ptr_in and operates with 
     a predetermined frame delay value set in its mm2s_frmdly_stride[28:24]
    
Dynamic Genlock Slave

    -   When configured as the Dynamic Genlock Slave, the channel
    accesses the previous frame that the Dynamic Genlock Master operated on. 
    
    To set up the channel in Dynamic Genlock Slave mode, the following settings should be
    used:
    - Set GenlockEn (MM2S_VDMACR[3]=1) to enable Genlock synchronization between
    master and slave.
    - Set GenlockSrc (MM2S_VDMACR[7]=1) to enable Internal Genlock mode. This bit is set
    to 1 by default if both channels are enabled in the Vivado IDE. When it is set to 1, user
    does not need to connect *_frame_ptr_out and *_frame_ptr_in signals
    externally. They are routed internally in the core.
