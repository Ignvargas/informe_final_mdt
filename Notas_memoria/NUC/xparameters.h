/* Definition for CPU ID */
#define XPAR_CPU_ID 0

/* Definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_PS7_CORTEXA9_0_CPU_CLK_FREQ_HZ 650000000


/******************************************************************/

/* Canonical definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_CPU_CORTEXA9_0_CPU_CLK_FREQ_HZ 650000000


/******************************************************************/

#include "xparameters_ps.h"

#define STDIN_BASEADDRESS 0xE0001000
#define STDOUT_BASEADDRESS 0xE0001000

/******************************************************************/

/* Definitions for driver CANVAS_MEMORYFRAME */
#define XPAR_CANVAS_MEMORYFRAME_NUM_INSTANCES 3

/* Definitions for peripheral CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0 */
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0_DEVICE_ID 0
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0_S00_AXI_BASEADDR 0x43C40000
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0_S00_AXI_HIGHADDR 0x43C4FFFF
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0_H_N_PIX 1440
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0_V_N_PIX 900
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0_H_FR_PIX 640
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_CANVAS_MEMORYFRAME_0_V_FR_PIX 512


/* Definitions for peripheral CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2 */
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2_DEVICE_ID 1
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2_S00_AXI_BASEADDR 0x43C60000
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2_S00_AXI_HIGHADDR 0x43C6FFFF
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2_H_N_PIX 1440
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2_V_N_PIX 900
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2_H_FR_PIX 640
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_CANVAS_MEMORYFRAME_2_V_FR_PIX 512


/* Definitions for peripheral CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1 */
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1_DEVICE_ID 2
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1_S00_AXI_BASEADDR 0x43C80000
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1_S00_AXI_HIGHADDR 0x43C8FFFF
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1_H_N_PIX 1440
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1_V_N_PIX 900
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1_H_FR_PIX 1280
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_TEXTFRAME_CANVAS_MEMORYFRAME_1_V_FR_PIX 300


/******************************************************************/

/* Definitions for driver CANVAS_SQUAREDRAW */
#define XPAR_CANVAS_SQUAREDRAW_NUM_INSTANCES 3

/* Definitions for peripheral CANVASOBJECTS_CANVAS_SQUAREDRAW_0 */
#define XPAR_CANVASOBJECTS_CANVAS_SQUAREDRAW_0_DEVICE_ID 0
#define XPAR_CANVASOBJECTS_CANVAS_SQUAREDRAW_0_S00_AXI_BASEADDR 0x43C10000
#define XPAR_CANVASOBJECTS_CANVAS_SQUAREDRAW_0_S00_AXI_HIGHADDR 0x43C1FFFF
#define XPAR_CANVASOBJECTS_CANVAS_SQUAREDRAW_0_H_N_PIX 1440
#define XPAR_CANVASOBJECTS_CANVAS_SQUAREDRAW_0_V_N_PIX 900


/* Definitions for peripheral CANVASOBJECTS_CAMERAICON */
#define XPAR_CANVASOBJECTS_CAMERAICON_DEVICE_ID 1
#define XPAR_CANVASOBJECTS_CAMERAICON_S00_AXI_BASEADDR 0x43C30000
#define XPAR_CANVASOBJECTS_CAMERAICON_S00_AXI_HIGHADDR 0x43C3FFFF
#define XPAR_CANVASOBJECTS_CAMERAICON_H_N_PIX 1440
#define XPAR_CANVASOBJECTS_CAMERAICON_V_N_PIX 900


/* Definitions for peripheral CANVASOBJECTS_ROI */
#define XPAR_CANVASOBJECTS_ROI_DEVICE_ID 2
#define XPAR_CANVASOBJECTS_ROI_S00_AXI_BASEADDR 0x43CC0000
#define XPAR_CANVASOBJECTS_ROI_S00_AXI_HIGHADDR 0x43CCFFFF
#define XPAR_CANVASOBJECTS_ROI_H_N_PIX 1440
#define XPAR_CANVASOBJECTS_ROI_V_N_PIX 900


/******************************************************************/

/* Definitions for driver CANVASBCD_DISPLAY */
#define XPAR_CANVASBCD_DISPLAY_NUM_INSTANCES 1

/* Definitions for peripheral CANVASOBJECTS_SPOTMETERDISPLAY_TEMPERATURA */
#define XPAR_CANVASOBJECTS_SPOTMETERDISPLAY_TEMPERATURA_DEVICE_ID 0
#define XPAR_CANVASOBJECTS_SPOTMETERDISPLAY_TEMPERATURA_S00_AXI_BASEADDR 0x43CB0000
#define XPAR_CANVASOBJECTS_SPOTMETERDISPLAY_TEMPERATURA_S00_AXI_HIGHADDR 0x43CBFFFF


/******************************************************************/

/* Definitions for driver CANVASHISTOGRAM */
#define XPAR_CANVASHISTOGRAM_NUM_INSTANCES 2

/* Definitions for peripheral CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST2_CANVASHISTOGRAM_0 */
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST2_CANVASHISTOGRAM_0_DEVICE_ID 0
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST2_CANVASHISTOGRAM_0_S00_AXI_BASEADDR 0x43C90000
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST2_CANVASHISTOGRAM_0_S00_AXI_HIGHADDR 0x43C9FFFF
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST2_CANVASHISTOGRAM_0_HIST_BINS 512
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST2_CANVASHISTOGRAM_0_HIST_HEIGHT 128


/* Definitions for peripheral CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST_1_CANVASHISTOGRAM_1 */
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST_1_CANVASHISTOGRAM_1_DEVICE_ID 1
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST_1_CANVASHISTOGRAM_1_S00_AXI_BASEADDR 0x43CA0000
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST_1_CANVASHISTOGRAM_1_S00_AXI_HIGHADDR 0x43CAFFFF
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST_1_CANVASHISTOGRAM_1_HIST_BINS 512
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_HISTOGRAM_HIST_1_CANVASHISTOGRAM_1_HIST_HEIGHT 128


/******************************************************************/

/* Definitions for driver COLORMAP */
#define XPAR_COLORMAP_NUM_INSTANCES 2

/* Definitions for peripheral CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_COLORMAP_0 */
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_COLORMAP_0_DEVICE_ID 0
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_COLORMAP_0_S00_AXI_BASEADDR 0x43C50000
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_CORRECTEDFRAME_COLORMAP_0_S00_AXI_HIGHADDR 0x43C5FFFF


/* Definitions for peripheral CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_COLORMAP_1 */
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_COLORMAP_1_DEVICE_ID 1
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_COLORMAP_1_S00_AXI_BASEADDR 0x43C70000
#define XPAR_CANVASOBJECTS_RAM_OBJECTS_RAWFRAME_COLORMAP_1_S00_AXI_HIGHADDR 0x43C7FFFF


/******************************************************************/

/* Definitions for driver CONFIG_GPIO */
#define XPAR_CONFIG_GPIO_NUM_INSTANCES 2

/* Definitions for peripheral ADRESSMANAGER */
#define XPAR_ADRESSMANAGER_DEVICE_ID 0
#define XPAR_ADRESSMANAGER_CONFIG_INTERFACE_BASEADDR 0x43C00000
#define XPAR_ADRESSMANAGER_CONFIG_INTERFACE_HIGHADDR 0x43C0FFFF


/* Definitions for peripheral CANVASOBJECTS_BCD_MANAGER */
#define XPAR_CANVASOBJECTS_BCD_MANAGER_DEVICE_ID 1
#define XPAR_CANVASOBJECTS_BCD_MANAGER_CONFIG_INTERFACE_BASEADDR 0x43C20000
#define XPAR_CANVASOBJECTS_BCD_MANAGER_CONFIG_INTERFACE_HIGHADDR 0x43C2FFFF


/******************************************************************/

/* Definitions for driver DEVCFG */
#define XPAR_XDCFG_NUM_INSTANCES 1

/* Definitions for peripheral PS7_DEV_CFG_0 */
#define XPAR_PS7_DEV_CFG_0_DEVICE_ID 0
#define XPAR_PS7_DEV_CFG_0_BASEADDR 0xF8007000
#define XPAR_PS7_DEV_CFG_0_HIGHADDR 0xF80070FF


/******************************************************************/

/* Canonical definitions for peripheral PS7_DEV_CFG_0 */
#define XPAR_XDCFG_0_DEVICE_ID XPAR_PS7_DEV_CFG_0_DEVICE_ID
#define XPAR_XDCFG_0_BASEADDR 0xF8007000
#define XPAR_XDCFG_0_HIGHADDR 0xF80070FF


/******************************************************************/

/* Definitions for driver DMAPS */
#define XPAR_XDMAPS_NUM_INSTANCES 2

/* Definitions for peripheral PS7_DMA_NS */
#define XPAR_PS7_DMA_NS_DEVICE_ID 0
#define XPAR_PS7_DMA_NS_BASEADDR 0xF8004000
#define XPAR_PS7_DMA_NS_HIGHADDR 0xF8004FFF


/* Definitions for peripheral PS7_DMA_S */
#define XPAR_PS7_DMA_S_DEVICE_ID 1
#define XPAR_PS7_DMA_S_BASEADDR 0xF8003000
#define XPAR_PS7_DMA_S_HIGHADDR 0xF8003FFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_DMA_NS */
#define XPAR_XDMAPS_0_DEVICE_ID XPAR_PS7_DMA_NS_DEVICE_ID
#define XPAR_XDMAPS_0_BASEADDR 0xF8004000
#define XPAR_XDMAPS_0_HIGHADDR 0xF8004FFF

/* Canonical definitions for peripheral PS7_DMA_S */
#define XPAR_XDMAPS_1_DEVICE_ID XPAR_PS7_DMA_S_DEVICE_ID
#define XPAR_XDMAPS_1_BASEADDR 0xF8003000
#define XPAR_XDMAPS_1_HIGHADDR 0xF8003FFF


/******************************************************************/

/* Definitions for driver EMACPS */
#define XPAR_XEMACPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_ETHERNET_0 */
#define XPAR_PS7_ETHERNET_0_DEVICE_ID 0
#define XPAR_PS7_ETHERNET_0_BASEADDR 0xE000B000
#define XPAR_PS7_ETHERNET_0_HIGHADDR 0xE000BFFF
#define XPAR_PS7_ETHERNET_0_ENET_CLK_FREQ_HZ 125000000
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV1 1
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV1 5
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV1 50


/******************************************************************/

/* Canonical definitions for peripheral PS7_ETHERNET_0 */
#define XPAR_XEMACPS_0_DEVICE_ID XPAR_PS7_ETHERNET_0_DEVICE_ID
#define XPAR_XEMACPS_0_BASEADDR 0xE000B000
#define XPAR_XEMACPS_0_HIGHADDR 0xE000BFFF
#define XPAR_XEMACPS_0_ENET_CLK_FREQ_HZ 125000000
#define XPAR_XEMACPS_0_ENET_SLCR_1000Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_1000Mbps_DIV1 1
#define XPAR_XEMACPS_0_ENET_SLCR_100Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_100Mbps_DIV1 5
#define XPAR_XEMACPS_0_ENET_SLCR_10Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_10Mbps_DIV1 50


/******************************************************************/


/* Definitions for peripheral PS7_AFI_0 */
#define XPAR_PS7_AFI_0_S_AXI_BASEADDR 0xF8008000
#define XPAR_PS7_AFI_0_S_AXI_HIGHADDR 0xF8008FFF


/* Definitions for peripheral PS7_AFI_1 */
#define XPAR_PS7_AFI_1_S_AXI_BASEADDR 0xF8009000
#define XPAR_PS7_AFI_1_S_AXI_HIGHADDR 0xF8009FFF


/* Definitions for peripheral PS7_AFI_2 */
#define XPAR_PS7_AFI_2_S_AXI_BASEADDR 0xF800A000
#define XPAR_PS7_AFI_2_S_AXI_HIGHADDR 0xF800AFFF


/* Definitions for peripheral PS7_AFI_3 */
#define XPAR_PS7_AFI_3_S_AXI_BASEADDR 0xF800B000
#define XPAR_PS7_AFI_3_S_AXI_HIGHADDR 0xF800BFFF


/* Definitions for peripheral PS7_CORESIGHT_COMP_0 */
#define XPAR_PS7_CORESIGHT_COMP_0_S_AXI_BASEADDR 0xF8800000
#define XPAR_PS7_CORESIGHT_COMP_0_S_AXI_HIGHADDR 0xF88FFFFF


/* Definitions for peripheral PS7_DDR_0 */
#define XPAR_PS7_DDR_0_S_AXI_BASEADDR 0x00100000
#define XPAR_PS7_DDR_0_S_AXI_HIGHADDR 0x1FFFFFFF


/* Definitions for peripheral PS7_DDRC_0 */
#define XPAR_PS7_DDRC_0_S_AXI_BASEADDR 0xF8006000
#define XPAR_PS7_DDRC_0_S_AXI_HIGHADDR 0xF8006FFF


/* Definitions for peripheral PS7_GLOBALTIMER_0 */
#define XPAR_PS7_GLOBALTIMER_0_S_AXI_BASEADDR 0xF8F00200
#define XPAR_PS7_GLOBALTIMER_0_S_AXI_HIGHADDR 0xF8F002FF


/* Definitions for peripheral PS7_GPV_0 */
#define XPAR_PS7_GPV_0_S_AXI_BASEADDR 0xF8900000
#define XPAR_PS7_GPV_0_S_AXI_HIGHADDR 0xF89FFFFF


/* Definitions for peripheral PS7_INTC_DIST_0 */
#define XPAR_PS7_INTC_DIST_0_S_AXI_BASEADDR 0xF8F01000
#define XPAR_PS7_INTC_DIST_0_S_AXI_HIGHADDR 0xF8F01FFF


/* Definitions for peripheral PS7_IOP_BUS_CONFIG_0 */
#define XPAR_PS7_IOP_BUS_CONFIG_0_S_AXI_BASEADDR 0xE0200000
#define XPAR_PS7_IOP_BUS_CONFIG_0_S_AXI_HIGHADDR 0xE0200FFF


/* Definitions for peripheral PS7_L2CACHEC_0 */
#define XPAR_PS7_L2CACHEC_0_S_AXI_BASEADDR 0xF8F02000
#define XPAR_PS7_L2CACHEC_0_S_AXI_HIGHADDR 0xF8F02FFF


/* Definitions for peripheral PS7_OCMC_0 */
#define XPAR_PS7_OCMC_0_S_AXI_BASEADDR 0xF800C000
#define XPAR_PS7_OCMC_0_S_AXI_HIGHADDR 0xF800CFFF


/* Definitions for peripheral PS7_PL310_0 */
#define XPAR_PS7_PL310_0_S_AXI_BASEADDR 0xF8F02000
#define XPAR_PS7_PL310_0_S_AXI_HIGHADDR 0xF8F02FFF


/* Definitions for peripheral PS7_PMU_0 */
#define XPAR_PS7_PMU_0_S_AXI_BASEADDR 0xF8891000
#define XPAR_PS7_PMU_0_S_AXI_HIGHADDR 0xF8891FFF
#define XPAR_PS7_PMU_0_PMU1_S_AXI_BASEADDR 0xF8893000
#define XPAR_PS7_PMU_0_PMU1_S_AXI_HIGHADDR 0xF8893FFF


/* Definitions for peripheral PS7_QSPI_LINEAR_0 */
#define XPAR_PS7_QSPI_LINEAR_0_S_AXI_BASEADDR 0xFC000000
#define XPAR_PS7_QSPI_LINEAR_0_S_AXI_HIGHADDR 0xFCFFFFFF


/* Definitions for peripheral PS7_RAM_0 */
#define XPAR_PS7_RAM_0_S_AXI_BASEADDR 0x00000000
#define XPAR_PS7_RAM_0_S_AXI_HIGHADDR 0x0003FFFF


/* Definitions for peripheral PS7_RAM_1 */
#define XPAR_PS7_RAM_1_S_AXI_BASEADDR 0xFFFC0000
#define XPAR_PS7_RAM_1_S_AXI_HIGHADDR 0xFFFFFFFF


/* Definitions for peripheral PS7_SCUC_0 */
#define XPAR_PS7_SCUC_0_S_AXI_BASEADDR 0xF8F00000
#define XPAR_PS7_SCUC_0_S_AXI_HIGHADDR 0xF8F000FC


/* Definitions for peripheral PS7_SLCR_0 */
#define XPAR_PS7_SLCR_0_S_AXI_BASEADDR 0xF8000000
#define XPAR_PS7_SLCR_0_S_AXI_HIGHADDR 0xF8000FFF


/******************************************************************/

/* Definitions for driver GPIOPS */
#define XPAR_XGPIOPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_GPIO_0 */
#define XPAR_PS7_GPIO_0_DEVICE_ID 0
#define XPAR_PS7_GPIO_0_BASEADDR 0xE000A000
#define XPAR_PS7_GPIO_0_HIGHADDR 0xE000AFFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_GPIO_0 */
#define XPAR_XGPIOPS_0_DEVICE_ID XPAR_PS7_GPIO_0_DEVICE_ID
#define XPAR_XGPIOPS_0_BASEADDR 0xE000A000
#define XPAR_XGPIOPS_0_HIGHADDR 0xE000AFFF


/******************************************************************/

/* Definitions for driver IICPS */
#define XPAR_XIICPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_I2C_0 */
#define XPAR_PS7_I2C_0_DEVICE_ID 0
#define XPAR_PS7_I2C_0_BASEADDR 0xE0004000
#define XPAR_PS7_I2C_0_HIGHADDR 0xE0004FFF
#define XPAR_PS7_I2C_0_I2C_CLK_FREQ_HZ 108333336


/******************************************************************/

/* Canonical definitions for peripheral PS7_I2C_0 */
#define XPAR_XIICPS_0_DEVICE_ID XPAR_PS7_I2C_0_DEVICE_ID
#define XPAR_XIICPS_0_BASEADDR 0xE0004000
#define XPAR_XIICPS_0_HIGHADDR 0xE0004FFF
#define XPAR_XIICPS_0_I2C_CLK_FREQ_HZ 108333336


/******************************************************************/

/* Definitions for driver QSPIPS */
#define XPAR_XQSPIPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_QSPI_0 */
#define XPAR_PS7_QSPI_0_DEVICE_ID 0
#define XPAR_PS7_QSPI_0_BASEADDR 0xE000D000
#define XPAR_PS7_QSPI_0_HIGHADDR 0xE000DFFF
#define XPAR_PS7_QSPI_0_QSPI_CLK_FREQ_HZ 200000000
#define XPAR_PS7_QSPI_0_QSPI_MODE 0


/******************************************************************/

/* Canonical definitions for peripheral PS7_QSPI_0 */
#define XPAR_XQSPIPS_0_DEVICE_ID XPAR_PS7_QSPI_0_DEVICE_ID
#define XPAR_XQSPIPS_0_BASEADDR 0xE000D000
#define XPAR_XQSPIPS_0_HIGHADDR 0xE000DFFF
#define XPAR_XQSPIPS_0_QSPI_CLK_FREQ_HZ 200000000
#define XPAR_XQSPIPS_0_QSPI_MODE 0


/******************************************************************/


/***Definitions for Core_nIRQ/nFIQ interrupts ****/
/* Definitions for Fabric interrupts connected to ps7_scugic_0 */
#define XPAR_FABRIC_TAU2MODULE_TAU2DRIVER_0_END_OF_FRAME_INTR 61
#define XPAR_FABRIC_VIDEODRIVER_PRIORITYDRAW_PRIORITY_DRAW_0_END_FRAME_INT_INTR 62
#define XPAR_FABRIC_TAU2MODULE_TAU2SERIALCOMMAND_0_NEW_TEMP_INTR 63
#define XPAR_FABRIC_MOUSE_PS2_MOUSE_INTERFACE_0_NEW_DATA_INTR 64

/******************************************************************/

/* Canonical definitions for Fabric interrupts connected to ps7_scugic_0 */

/******************************************************************/

/* Definitions for driver SCUGIC */
#define XPAR_XSCUGIC_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUGIC_0 */
#define XPAR_PS7_SCUGIC_0_DEVICE_ID 0
#define XPAR_PS7_SCUGIC_0_BASEADDR 0xF8F00100
#define XPAR_PS7_SCUGIC_0_HIGHADDR 0xF8F001FF
#define XPAR_PS7_SCUGIC_0_DIST_BASEADDR 0xF8F01000


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUGIC_0 */
#define XPAR_SCUGIC_0_DEVICE_ID 0
#define XPAR_SCUGIC_0_CPU_BASEADDR 0xF8F00100
#define XPAR_SCUGIC_0_CPU_HIGHADDR 0xF8F001FF
#define XPAR_SCUGIC_0_DIST_BASEADDR 0xF8F01000


/******************************************************************/

/* Definitions for driver SCUTIMER */
#define XPAR_XSCUTIMER_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUTIMER_0 */
#define XPAR_PS7_SCUTIMER_0_DEVICE_ID 0
#define XPAR_PS7_SCUTIMER_0_BASEADDR 0xF8F00600
#define XPAR_PS7_SCUTIMER_0_HIGHADDR 0xF8F0061F


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUTIMER_0 */
#define XPAR_XSCUTIMER_0_DEVICE_ID XPAR_PS7_SCUTIMER_0_DEVICE_ID
#define XPAR_XSCUTIMER_0_BASEADDR 0xF8F00600
#define XPAR_XSCUTIMER_0_HIGHADDR 0xF8F0061F


/******************************************************************/

/* Definitions for driver SCUWDT */
#define XPAR_XSCUWDT_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUWDT_0 */
#define XPAR_PS7_SCUWDT_0_DEVICE_ID 0
#define XPAR_PS7_SCUWDT_0_BASEADDR 0xF8F00620
#define XPAR_PS7_SCUWDT_0_HIGHADDR 0xF8F006FF


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUWDT_0 */
#define XPAR_SCUWDT_0_DEVICE_ID XPAR_PS7_SCUWDT_0_DEVICE_ID
#define XPAR_SCUWDT_0_BASEADDR 0xF8F00620
#define XPAR_SCUWDT_0_HIGHADDR 0xF8F006FF


/******************************************************************/

/* Definitions for driver SDPS */
#define XPAR_XSDPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SD_0 */
#define XPAR_PS7_SD_0_DEVICE_ID 0
#define XPAR_PS7_SD_0_BASEADDR 0xE0100000
#define XPAR_PS7_SD_0_HIGHADDR 0xE0100FFF
#define XPAR_PS7_SD_0_SDIO_CLK_FREQ_HZ 50000000


/******************************************************************/

/* Canonical definitions for peripheral PS7_SD_0 */
#define XPAR_XSDPS_0_DEVICE_ID XPAR_PS7_SD_0_DEVICE_ID
#define XPAR_XSDPS_0_BASEADDR 0xE0100000
#define XPAR_XSDPS_0_HIGHADDR 0xE0100FFF
#define XPAR_XSDPS_0_SDIO_CLK_FREQ_HZ 50000000


/******************************************************************/

/* Definitions for driver UARTPS */
#define XPAR_XUARTPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_UART_1 */
#define XPAR_PS7_UART_1_DEVICE_ID 0
#define XPAR_PS7_UART_1_BASEADDR 0xE0001000
#define XPAR_PS7_UART_1_HIGHADDR 0xE0001FFF
#define XPAR_PS7_UART_1_UART_CLK_FREQ_HZ 50000000
#define XPAR_PS7_UART_1_HAS_MODEM 0


/******************************************************************/

/* Canonical definitions for peripheral PS7_UART_1 */
#define XPAR_XUARTPS_0_DEVICE_ID XPAR_PS7_UART_1_DEVICE_ID
#define XPAR_XUARTPS_0_BASEADDR 0xE0001000
#define XPAR_XUARTPS_0_HIGHADDR 0xE0001FFF
#define XPAR_XUARTPS_0_UART_CLK_FREQ_HZ 50000000
#define XPAR_XUARTPS_0_HAS_MODEM 0


/******************************************************************/

/* Definitions for driver USBPS */
#define XPAR_XUSBPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_USB_0 */
#define XPAR_PS7_USB_0_DEVICE_ID 0
#define XPAR_PS7_USB_0_BASEADDR 0xE0002000
#define XPAR_PS7_USB_0_HIGHADDR 0xE0002FFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_USB_0 */
#define XPAR_XUSBPS_0_DEVICE_ID XPAR_PS7_USB_0_DEVICE_ID
#define XPAR_XUSBPS_0_BASEADDR 0xE0002000
#define XPAR_XUSBPS_0_HIGHADDR 0xE0002FFF


/******************************************************************/

/* Definitions for driver XADCPS */
#define XPAR_XADCPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_XADC_0 */
#define XPAR_PS7_XADC_0_DEVICE_ID 0
#define XPAR_PS7_XADC_0_BASEADDR 0xF8007100
#define XPAR_PS7_XADC_0_HIGHADDR 0xF8007120


/******************************************************************/

/* Canonical definitions for peripheral PS7_XADC_0 */
#define XPAR_XADCPS_0_DEVICE_ID XPAR_PS7_XADC_0_DEVICE_ID
#define XPAR_XADCPS_0_BASEADDR 0xF8007100
#define XPAR_XADCPS_0_HIGHADDR 0xF8007120


/******************************************************************/

/* Xilinx FAT File System Library (XilFFs) User Settings */
#define FILE_SYSTEM_INTERFACE_SD
