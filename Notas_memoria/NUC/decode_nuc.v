wire  [0:0]   ERODE = ((PIXEL_LEARN_0 != 3'b111) || (PIXEL_LEARN_1 != 3'b111)
                         || (PIXEL_LEARN_2 != 3'b111));
assign OUTPUT = (BINARIZE==1 )? (ERODE? 0:8191): Add_Stage_2_SN>>>4;

    ...

if (RESET_REQUEST)    
    begin
        Add_Stage_2_SN          <= (PIXEL_SIGNED_PIP_1<<<4);
        Add_Stage_2_SN_Shifted  <= (PIXEL_SIGNED_PIP_1<<<6);
    end
else
    begin
        Add_Stage_2_SN          <= (PIXEL_SIGNED_PIP_1<<<4) + ((Add_Stage_1+Add_Stage_1_aux 
                                    +Add_Stage_1_comp)<<<2) + S_NOISE_PIP_1;
                                    
        Add_Stage_2_SN_Shifted  <= ((PIXEL_SIGNED_PIP_1<<<4) + ((Add_Stage_1+Add_Stage_1_aux 
                                    +Add_Stage_1_comp)<<<2) + S_NOISE_PIP_1)>>>2;
    end

    ** PIXEL_SIGNED_PIP_1 es el valor del pixel actual. (Dentro de un registro de desplazamiento.)
    
    ** Add_Stage_1      <= (Mult_stage_1>>>18) + (Mult_stage_2>>>18);
    
    ** Add_Stage_1_aux  <= (Mult_stage_3>>>18) + (Mult_stage_4>>>18);
    
    ** Add_Stage_1_comp <= (Mult_stage_5>>>18) + (Mult_stage_6>>>18);
    
    ...
    
    Mult_stage_1        <= Shape1K*Curve1_Data;

    Mult_stage_2        <= Shape2K*Curve2_Data;

    Mult_stage_3        <= Shape3K*Curve3_Data;

    Mult_stage_4        <= Shape4K*Curve4_Data;

    Mult_stage_5        <= Shape5K*Curve5_Data;

    Mult_stage_6        <= Shape6K*Curve6_Data;
    
    //Curve1_Data son datos de 16 bits que llegan al modulo.
    
    ...
    
    //Buscando Shape1K =D     .......
    
    Shape1K  <=  Shape1K_PIP;

    Shape2K  <=  Shape2K_PIP;

    Shape3K  <=  Shape3K_PIP;

    Shape4K  <=  Shape4K_PIP;

    Shape5K  <=  Shape5K_PIP;

    Shape6K  <=  Shape6K_PIP;
    
            ....
            
    if (UPDATE_SHAPE_PIP2) begin

        if (RESET_REQUEST)	

            begin

                RESET_REQUEST <= 0;

                Shape1K_PIP <= 0;

                Shape2K_PIP <= 0;

                Shape3K_PIP <= 0;

                Shape4K_PIP <= 0;

                Shape5K_PIP <= 0;

                Shape6K_PIP <= 0;

            end

        else if (NUC_LEARN) begin

            
            Shape1K_PIP <= Shape1K - Shape1K_MULT_PIP2;

            Shape2K_PIP <= Shape2K - Shape2K_MULT_PIP2;

            Shape3K_PIP <= Shape3K - Shape3K_MULT_PIP2;

            Shape4K_PIP <= Shape4K - Shape4K_MULT_PIP2;

            Shape5K_PIP <= Shape5K - Shape5K_MULT_PIP2;

            Shape6K_PIP <= Shape6K - Shape6K_MULT_PIP2;

        end

    end        

            ....
            
    if (UPDATE_SHAPE)

            UPDATE_SHAPE <= 0;

        
    UPDATE_SHAPE_PIP <=  UPDATE_SHAPE;
                    
    //MultyCiclePath(?)     

    Shape1K_MULT_PIP <=  Shape1K_MULT;

    Shape2K_MULT_PIP <=  Shape2K_MULT;

    Shape3K_MULT_PIP <=  Shape3K_MULT;

    Shape4K_MULT_PIP <=  Shape4K_MULT;

    Shape5K_MULT_PIP <=  Shape5K_MULT;

    Shape6K_MULT_PIP <=  Shape6K_MULT;

        
        
    UPDATE_SHAPE_PIP2 <=  UPDATE_SHAPE_PIP;
                    
    //MultyCiclePath(?                         

    Shape1K_MULT_PIP2 <=  Shape1K_MULT_PIP;

    Shape2K_MULT_PIP2 <=  Shape2K_MULT_PIP;

    Shape3K_MULT_PIP2 <=  Shape3K_MULT_PIP;

    Shape4K_MULT_PIP2 <=  Shape4K_MULT_PIP;

    Shape5K_MULT_PIP2 <=  Shape5K_MULT_PIP;

    Shape6K_MULT_PIP2 <=  Shape6K_MULT_PIP;
    
            ....
            
    
    if (UPDATE_MULT) begin

        UPDATE_MULT <= 0;

        if (RESET_REQUEST)

            begin

                Shape1K_MULT <= 0;

                Shape2K_MULT <= 0;

                Shape3K_MULT <= 0;

                Shape4K_MULT <= 0;

                Shape5K_MULT <= 0;

                Shape6K_MULT <= 0;

            end

        else

            begin

                Shape1K_MULT <= ((Eta_1_pip*SUM_CURVE_1_TOT)>>>8);

                Shape2K_MULT <= ((Eta_2_pip*SUM_CURVE_2_TOT)>>>8);

                Shape3K_MULT <= ((Eta_3_pip*SUM_CURVE_3_TOT)>>>8);

                Shape4K_MULT <= ((Eta_4_pip*SUM_CURVE_4_TOT)>>>8);

                Shape5K_MULT <= ((Eta_5_pip*SUM_CURVE_5_TOT)>>>8);

                Shape6K_MULT <= ((Eta_6_pip*SUM_CURVE_6_TOT)>>>8);

            end

        UPDATE_SHAPE <= 1;

    end
    // Eta_i_pip es un registro de desplazamiento que proviene de Eta_1, una entrada al modulo de 32 bits

        ....
    
    //OPERACIONES ARITMETICAS

    if (VALID_DATA_PIP_7 == 1  &&  VALID_DATA_PIP_6 == 0  &&  VALID_DATA_PIP_5 == 0)

        begin

            if (LineCounter == LINES-3)

                begin

                    SUM_CURVE_1_TOT <= CURVE_1_COL_SUM>>>5;

                    SUM_CURVE_2_TOT <= CURVE_2_COL_SUM>>>5;

                    SUM_CURVE_3_TOT <= CURVE_3_COL_SUM>>>5;

                    SUM_CURVE_4_TOT <= CURVE_4_COL_SUM>>>5;

                    SUM_CURVE_5_TOT <= CURVE_5_COL_SUM>>>5;

                    SUM_CURVE_6_TOT <= CURVE_6_COL_SUM>>>5;
                    UPDATE_MULT <= 1;

                end

        end

    else

        begin

            SUM_CURVE_1_TOT <= 0;

            SUM_CURVE_2_TOT <= 0;

            SUM_CURVE_3_TOT <= 0;

            SUM_CURVE_4_TOT <= 0;

            SUM_CURVE_5_TOT <= 0;

            SUM_CURVE_6_TOT <= 0;
        end

        ....
        
    if (VALID_DATA_PIP_6 == 1  &&  VALID_DATA_PIP_5 == 0) //Ultimo elemento de la fila.

        begin
            //CURVE_1_COL_SUM es de 5 bits fraccionarios.

            //La operacion es CURVE_1_ROW_SUM/512. CURVE_1_ROW_SUM es de 4 bits fraccionarios.

            //Para convertirlo a 5 bits, equivale a hacer CURVE_1_ROW_SUM/256

            CURVE_1_COL_SUM <= CURVE_1_COL_SUM + (CURVE_1_ROW_SUM>>>8);

            //CURVE_2_COL_SUM es de 6 bits fraccionarios.

            //La operacion es CURVE_2_ROW_SUM/131072 (dividido por 2^17). CURVE_2_ROW_SUM es de 0 bits fraccionarios.

            //Para convertirlo a 6 bits, equivale a hacer CURVE_2_ROW_SUM/2048

            CURVE_2_COL_SUM <= CURVE_2_COL_SUM + (CURVE_2_ROW_SUM>>>8);
            CURVE_3_COL_SUM <= CURVE_3_COL_SUM + (CURVE_3_ROW_SUM>>>8);

            CURVE_4_COL_SUM <= CURVE_4_COL_SUM + (CURVE_4_ROW_SUM>>>8);

            CURVE_5_COL_SUM <= CURVE_5_COL_SUM + (CURVE_5_ROW_SUM>>>8);

            CURVE_6_COL_SUM <= CURVE_6_COL_SUM + (CURVE_6_ROW_SUM>>>8);
        end

    else if (VALID_DATA_PIP_7 == 1  && VALID_DATA_PIP_6 == 0  &&  VALID_DATA_PIP_5 == 0)

        begin

            if (LineCounter == LINES-3) // Fin de lineas (Frames)
 
                begin

                    LineCounter <= 0;

                    CURVE_1_COL_SUM <= 0;
                    CURVE_2_COL_SUM <= 0;

                    CURVE_3_COL_SUM <= 0; 

                    CURVE_4_COL_SUM <= 0;  

                    CURVE_5_COL_SUM <= 0; 

                    CURVE_6_COL_SUM <= 0;
                end

            else

                begin

                    LineCounter <= LineCounter  + 1'b1; //Contador de lineas

                end

        end
        
        // Acumulador vertical. dCF/dfu y dCF/du.
        
        ...
        
    if (VALID_DATA_PIP_5) 

        begin

            CURVE_1_ROW_SUM    <= CURVE_1_ROW_SUM + CURVE_1_ROW_MULT; 

            CURVE_2_ROW_SUM    <= CURVE_2_ROW_SUM + CURVE_2_ROW_MULT;

            CURVE_3_ROW_SUM    <= CURVE_3_ROW_SUM + CURVE_3_ROW_MULT;

            CURVE_4_ROW_SUM    <= CURVE_4_ROW_SUM + CURVE_4_ROW_MULT; 

            CURVE_5_ROW_SUM    <= CURVE_5_ROW_SUM + CURVE_5_ROW_MULT;

            CURVE_6_ROW_SUM    <= CURVE_6_ROW_SUM + CURVE_6_ROW_MULT;
        end

    else

        begin

            CURVE_1_ROW_SUM    <= 0; 

            CURVE_2_ROW_SUM    <= 0;

            CURVE_3_ROW_SUM    <= 0;

            CURVE_4_ROW_SUM    <= 0; 

            CURVE_5_ROW_SUM    <= 0;

            CURVE_6_ROW_SUM    <= 0;

        end
        
        // Acumulador horizontal !!!!
        
        ....
        
    if (VALID_DATA_PIP_4) 

        begin
            //CURVE_1_ROW_MULT	<= (2*(Expected*Curve_1_PIP_3)>>>13); 	// 11 bits punto fijo por 6 bits de punto fijo: objetivo 4			

            CURVE_1_ROW_MULT    <= ((Expected*Curve_1_PIP_4)>>>7); 	

            //CURVE_2_ROW_MULT 	<= (2*(Expected*Curve_2_PIP_3)>>>11);  	// 11 bits punto fijo por 0 bits de punto fijo: objetivo 0		

            CURVE_2_ROW_MULT    <= ((Expected*Curve_2_PIP_4)>>>7);

            CURVE_3_ROW_MULT    <= ((Expected*Curve_3_PIP_4)>>>7);

            CURVE_4_ROW_MULT    <= ((Expected*Curve_4_PIP_4)>>>7); 

            CURVE_5_ROW_MULT    <= ((Expected*Curve_5_PIP_4)>>>7);

            CURVE_6_ROW_MULT    <= ((Expected*Curve_6_PIP_4)>>>7);
        end

    else begin

            CURVE_1_ROW_MULT    <= 0;
            CURVE_2_ROW_MULT    <= 0;

            CURVE_3_ROW_MULT    <= 0; 

            CURVE_4_ROW_MULT    <= 0;

            CURVE_5_ROW_MULT    <= 0; 

            CURVE_6_ROW_MULT    <= 0;    

    end
    // Curve_1_PIP_3 proviene de un registro de desplazamiento, cuya fuente es la entrada Curve_1 del modulo.
    
    // CURVE_1_ROW_MULT = [X^(i,j)-E( X^i,j)] * S^(i,j)
    // >>> 7 Factor de escala disminuir la cantidad de registros utilizados.
    ....
    
    if (VALID_DATA_PIP_3)

        begin

            //14 bits de punto fijo. hay que pasarlos a 11

            Expected        <= (Expected_Mult);     // X^(i,j)-E(Xi,j)

        end
    
    ....
    
    //12 bits de punto fijo por 2 bits de punto fijo. 14 bits de punto fijo total

    wire signed [32:0] Expected_Mult = ((CF_Diff_PIP_3 - PIXEL_TOTAL));
    
    ....
    
    *** PIXEL_TOTAL ***
    // E(X^i,j)

    *** CF_Diff_PIP_3 ***
    
    CF_Diff_PIP_3       <= CF_Diff_PIP_2;
    CF_Diff_PIP_2       <= CF_Diff_PIP_1;
    CF_Diff_PIP_1       <= ($signed(PIXEL[1][1])<<<3) + ($signed(PIXEL[1][1])<<<2); 
    
    
    
        
    
