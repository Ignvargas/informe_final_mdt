wire  [0:0]   ERODE = ((PIXEL_LEARN_0 != 3'b111) || (PIXEL_LEARN_1 != 3'b111) 
                        || (PIXEL_LEARN_2 != 3'b111));

assign OUTPUT = (BINARIZE==1 )? (ERODE? 0:8191): Add_Stage_2_SN>>>4; 

if (RESET_REQUEST)
    begin
        Add_Stage_2_SN			<= (PIXEL_SIGNED_PIP_1<<<4);
        Add_Stage_2_SN_Shifted  <= (PIXEL_SIGNED_PIP_1<<<6);
        end
    else
    begin
        Add_Stage_2_SN          <= (PIXEL_SIGNED_PIP_1<<<4) + ((Add_Stage_1+Add_Stage_1_aux 
                                    +Add_Stage_1_comp)<<<2) + S_NOISE_PIP_1;
        Add_Stage_2_SN_Shifted  <= ((PIXEL_SIGNED_PIP_1<<<4) + ((Add_Stage_1+Add_Stage_1_a
                                    +Add_Stage_1_comp)<<<2) + S_NOISE_PIP_1)>>>2;
    end
    
------

S_NOISE_PIP_1			<= S_NOISE_PIP_0>>>3;
SNOISE_1                <= S_NOISE_PIP_0; // Stripping_noise_Buffer[HPOS_PIP_0] ?;

------

S_NOISE_PIP_0			<= Stripping_noise_Buffer[HPOS];

------

if (VALID_LINE_PIP_1)
begin
    //4 bits punto fijo.
    if (DIFF_PIXELS > 32'sd32)
        Stripping_noise[HPOS_PIP_6] <= (VPOS_PIP_6==1? 32'sd0:Stripping_noise_read) + 32'sd32;
    else if (DIFF_PIXELS < -32'sd32)
        Stripping_noise[HPOS_PIP_6] <= (VPOS_PIP_6==1? 32'sd0:Stripping_noise_read) -32'sd32;
    else
        Stripping_noise[HPOS_PIP_6] <= (VPOS_PIP_6==1? 32'sd0:Stripping_noise_read) + DIFF_PIXELS;

    if (RESET_REQUEST)
        begin
            Stripping_noise_Buffer[HPOS_PIP_5] <= 0;
        end
    else if (VPOS_PIP_6 == 511 && STRIPPING_ENABLE && NUC_LEARN)
        begin
            //Stripping_noise_Buffer[HPOS_PIP_5] <= SNOISE_4 -((Stripping_noise[HPOS_PIP_6]  )>>>6);
            //Stripping_noise_Buffer[HPOS_PIP_5] <= SNOISE_5 -((Stripping_noise[HPOS_PIP_6]  )>>>7);
            Stripping_noise_Buffer[HPOS_PIP_5] <= SNOISE_5 -(Stripping_noise_read>>>6);
        end

end

-------

SNOISE_2                <= SNOISE_1; // Stripping_noise_Buffer[HPOS_PIP_1] ?;
//etapa 4 pipeline (stripping noise)
HPOS_PIP_3 <= HPOS_PIP_2;
VPOS_PIP_3 <= VPOS_PIP_2;
WRITE_ADDR <= HPOS_PIP_2;
SNOISE_3               <=  SNOISE_2; // Stripping_noise_Buffer[HPOS_PIP_2] ?;


//etapa 5 pipeline (stripping noise)
HPOS_PIP_4 <= HPOS_PIP_3;
VPOS_PIP_4 <= VPOS_PIP_3;
SNOISE_4               <=  SNOISE_3; // Stripping_noise_Buffer[HPOS_PIP_3] ?;

//etapa 6 pipeline (stripping noise)
HPOS_PIP_5 <= HPOS_PIP_4;
VPOS_PIP_5 <= VPOS_PIP_4;
SNOISE_5               <=  SNOISE_4; // Stripping_noise_Buffer[HPOS_PIP_4] ?;
//etapa 7 pipeline (stripping noise)
HPOS_PIP_6 <= HPOS_PIP_5;
VPOS_PIP_6 <= VPOS_PIP_5;
SNOISE_6               <=  SNOISE_5; // Stripping_noise_Buffer[HPOS_PIP_5] ?;

------

SNOISE_1                <= S_NOISE_PIP_0; // Stripping_noise_Buffer[HPOS_PIP_0] ?;

------

Stripping_noise_read_pip <=  Stripping_noise[HPOS_PIP_4];
Stripping_noise_read <=  Stripping_noise_read_pip;

------

if (VALID_LINE_PIP_0)
begin
    if (ERODE == 1'b1)
        DIFF_PIXELS 			<= 0;
    else
        begin
            DIFF_PIXELS 			<= (StripDiff*DIV_BY_3)>>>14;
            //DIFF_PIXELS <= (((PIXEL_SN[1]<<<1) - PIXEL_SN[2] - PIXEL_SN[0])*DIV_BY_3)>>>14;
        end

end

------

StripDiff <= 	(($signed(PIXEL_SN[2])<<<1) - Add_stage_2_sn_read - $signed(PIXEL_SN[1]));

------

if (NEW_PIXEL_PIP_2)
    begin
    
    .....
    
if (HPOS_PIP_3 == 0 && HPOS_PIP_2 != 0)
    begin
        PIXEL_SN[0]			<= Add_stage_2_sn_read;
        PIXEL_SN[1]			<= Add_stage_2_sn_read;


        PIXEL_LEARN_0       <= {3{BIN_0_Read}};
        PIXEL_LEARN_1       <= {3{BIN_1_Read}};
        //PIXEL_LEARN_2       <= {3{OUTPUT_BIN}};

    end
else
    begin
        PIXEL_SN[0]			<= PIXEL_SN[1];
        PIXEL_SN[1]			<= PIXEL_SN[2];

        PIXEL_LEARN_0[2:0]  <= {BIN_0_Read,PIXEL_LEARN_0[2:1]};
        PIXEL_LEARN_1[2:0]  <= {BIN_1_Read,PIXEL_LEARN_1[2:1]};


    end
    
if (HPOS_PIP_3 != HPOS_PIP_2)
    begin
        PIXEL_SN[2]            <= Add_stage_2_sn_read;
        PIXEL_LEARN_2[2:0]     <= {OUTPUT_BIN,PIXEL_LEARN_2[2:1]};
    end
    
    ....

end
