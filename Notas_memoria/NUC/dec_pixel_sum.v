 *** PIXEL_TOTAL ***
    
    if (VALID_DATA_PIP_2) 
        begin
            PIXEL_TOTAL     <=  $signed(PIXEL_SUM_PIP_C[0]) + $signed(PIXEL_SUM_PIP_C[1]);
        end
        
------

    if (VALID_DATA_PIP_1)
        begin
            PIXEL_SUM_PIP_C[0] 	<= $signed(PIXEL_SUM_PIP_A[0]) + $signed(PIXEL_SUM_PIP_A[1]);
            PIXEL_SUM_PIP_C[1] 	<= $signed(PIXEL_SUM_PIP_B[0]) + $signed(PIXEL_SUM_PIP_B[1]);
        end
        
------

if (VALID_DATA_PIP_0)
    begin
        PIXEL_SUM_PIP_A[0] 	<= $signed(PIXEL[0][0]) + ($signed(PIXEL[0][1])<<<1);
        PIXEL_SUM_PIP_A[1] 	<= $signed(PIXEL[0][2]) + ($signed(PIXEL[1][0])<<<1);
            
        PIXEL_SUM_PIP_B[0]	<= ($signed(PIXEL[1][2])<<<1) + $signed(PIXEL[2][0]);
        PIXEL_SUM_PIP_B[1]	<= ($signed(PIXEL[2][1])<<<1) + $signed(PIXEL[2][2]);
            
            
            
    end

------

    PIXEL[0][0]			<= PIXEL[1][0];
    PIXEL[1][0]			<= PIXEL[2][0];
    PIXEL[2][0]			<= LineBuffer1_Read;

    PIXEL[0][1]			<= PIXEL[1][1];
    PIXEL[1][1]			<= PIXEL[2][1];
    PIXEL[2][1]			<= LineBuffer2_Read;

    PIXEL[0][2]			<= PIXEL[1][2];
    PIXEL[1][2]			<= PIXEL[2][2];
    PIXEL[2][2]			<= USE_SNOISE? Add_Stage_2_SN_Shifted:Add_Stage_2;
    
------

    //Equivale a LineBuffer1_Read <= LineBuffer1[HPOS_PIP_2];
    LineBuffer1_Read_PIP            <= LineBuffer1[HPOS_PIP_1];
    LineBuffer1_Read                <= LineBuffer1_Read_PIP;
        
    //Equivale a LineBuffer2_Read <= LineBuffer2[HPOS_PIP_2];
    LineBuffer2_Read_PIP            <= LineBuffer2[HPOS_PIP_1];
    LineBuffer2_Read                <= LineBuffer2_Read_PIP;

------
    //Lee datos desde archivo ZeroGen.mif ... ??
    $readmemh("ZeroGen.mif", LineBuffer1, 0, 639);
    $readmemh("ZeroGen.mif", LineBuffer2, 0, 639);

    
    
**************

Suposiciones ...
    -   Eta_1...6 tiene relacion con las tasas de aprendizaje.
    -   Curve1_Data...6 tiene relacion con los valores obtenidos de la calibracion. S1^(i,j) S2^(i,j)
    -   Shape1k     Guarda el valor u[k] y f[k] del frame anterior. ( u[k-1] y f[k-1] )

Expected = X^(i,j)-E(Xi,j)

CURVE_1_ROW_MULT = Expected * S^(i,j)  // Se acmula por cada elemento de fila, y luego por columna.

SUM_CURVE_1_TOT = CURVE_1_COL_SUM = Sum(i, j) [ Expected * S^(i,j) ] // Esta listo, Una vez que se acumulan todos los 
                                                                     // valores del frame. dFC/dfo, dFC/du.

Shape1K_MULT = n_2 * dFC/du

Shape1K_PIP <= Shape1K - Shape1K_MULT_PIP2  // u[k] y fo[k]

Shape1K  <=  Shape1K_PIP;

Mult_stage_1        <= Shape1K*Curve1_Data;
