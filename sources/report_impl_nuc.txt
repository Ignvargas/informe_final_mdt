Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
--------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2016.1 (lin64) Build 1538259 Fri Apr  8 15:45:23 MDT 2016
| Date         : Thu May 25 00:50:43 2017
| Host         : linux-n52f running 64-bit openSUSE Leap 42.1 (x86_64)
| Command      : report_utilization -file /home/Nacho/Dev/report_impl_nuc.txt -name utilization_1
| Design       : system_top
| Device       : 7z020clg484-1
| Design State : Routed
--------------------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Slice Logic
1.1 Summary of Registers by Type
2. Slice Logic Distribution
3. Memory
4. DSP
5. IO and GT Specific
6. Clocking
7. Specific Feature
8. Primitives
9. Black Boxes
10. Instantiated Netlists

1. Slice Logic
--------------

+----------------------------+-------+-------+-----------+-------+
|          Site Type         |  Used | Fixed | Available | Util% |
+----------------------------+-------+-------+-----------+-------+
| Slice LUTs                 | 17739 |     0 |     53200 | 33.34 |
|   LUT as Logic             | 16583 |     0 |     53200 | 31.17 |
|   LUT as Memory            |  1156 |     0 |     17400 |  6.64 |
|     LUT as Distributed RAM |   198 |     0 |           |       |
|     LUT as Shift Register  |   958 |     0 |           |       |
| Slice Registers            | 22129 |     0 |    106400 | 20.80 |
|   Register as Flip Flop    | 22129 |     0 |    106400 | 20.80 |
|   Register as Latch        |     0 |     0 |    106400 |  0.00 |
| F7 Muxes                   |    56 |     0 |     26600 |  0.21 |
| F8 Muxes                   |     4 |     0 |     13300 |  0.03 |
+----------------------------+-------+-------+-----------+-------+


1.1 Summary of Registers by Type
--------------------------------

+-------+--------------+-------------+--------------+
| Total | Clock Enable | Synchronous | Asynchronous |
+-------+--------------+-------------+--------------+
| 0     |            _ |           - |            - |
| 0     |            _ |           - |          Set |
| 0     |            _ |           - |        Reset |
| 0     |            _ |         Set |            - |
| 0     |            _ |       Reset |            - |
| 0     |          Yes |           - |            - |
| 107   |          Yes |           - |          Set |
| 1350  |          Yes |           - |        Reset |
| 708   |          Yes |         Set |            - |
| 19983 |          Yes |       Reset |            - |
+-------+--------------+-------------+--------------+


2. Slice Logic Distribution
---------------------------

+------------------------------------------+-------+-------+-----------+-------+
|                 Site Type                |  Used | Fixed | Available | Util% |
+------------------------------------------+-------+-------+-----------+-------+
| Slice                                    |  6722 |     0 |     13300 | 50.54 |
|   SLICEL                                 |  4418 |     0 |           |       |
|   SLICEM                                 |  2304 |     0 |           |       |
| LUT as Logic                             | 16583 |     0 |     53200 | 31.17 |
|   using O5 output only                   |     2 |       |           |       |
|   using O6 output only                   | 13580 |       |           |       |
|   using O5 and O6                        |  3001 |       |           |       |
| LUT as Memory                            |  1156 |     0 |     17400 |  6.64 |
|   LUT as Distributed RAM                 |   198 |     0 |           |       |
|     using O5 output only                 |     0 |       |           |       |
|     using O6 output only                 |    26 |       |           |       |
|     using O5 and O6                      |   172 |       |           |       |
|   LUT as Shift Register                  |   958 |     0 |           |       |
|     using O5 output only                 |     8 |       |           |       |
|     using O6 output only                 |   369 |       |           |       |
|     using O5 and O6                      |   581 |       |           |       |
| LUT Flip Flop Pairs                      |  9207 |     0 |     53200 | 17.31 |
|   fully used LUT-FF pairs                |  1603 |       |           |       |
|   LUT-FF pairs with one unused LUT       |  6951 |       |           |       |
|   LUT-FF pairs with one unused Flip Flop |  6877 |       |           |       |
| Unique Control Sets                      |  1131 |       |           |       |
+------------------------------------------+-------+-------+-----------+-------+
* Note: Review the Control Sets Report for more information regarding control sets.


3. Memory
---------

+-------------------+------+-------+-----------+-------+
|     Site Type     | Used | Fixed | Available | Util% |
+-------------------+------+-------+-----------+-------+
| Block RAM Tile    | 21.5 |     0 |       140 | 15.36 |
|   RAMB36/FIFO*    |   13 |     0 |       140 |  9.29 |
|     RAMB36E1 only |   13 |       |           |       |
|   RAMB18          |   17 |     0 |       280 |  6.07 |
|     RAMB18E1 only |   17 |       |           |       |
+-------------------+------+-------+-----------+-------+
* Note: Each Block RAM Tile only has one FIFO logic available and therefore can accommodate only one FIFO36E1 or one FIFO18E1. However, if a FIFO18E1 occupies a Block RAM Tile, that tile can still accommodate a RAMB18E1


4. DSP
------

+----------------+------+-------+-----------+-------+
|    Site Type   | Used | Fixed | Available | Util% |
+----------------+------+-------+-----------+-------+
| DSPs           |   57 |     0 |       220 | 25.91 |
|   DSP48E1 only |   57 |       |           |       |
+----------------+------+-------+-----------+-------+


5. IO and GT Specific
---------------------

+-----------------------------+------+-------+-----------+--------+
|          Site Type          | Used | Fixed | Available |  Util% |
+-----------------------------+------+-------+-----------+--------+
| Bonded IOB                  |   85 |    85 |       200 |  42.50 |
|   IOB Master Pads           |   38 |       |           |        |
|   IOB Slave Pads            |   39 |       |           |        |
|   IOB Flip Flops            |   19 |    19 |           |        |
| Bonded IPADs                |    0 |     0 |         2 |   0.00 |
| Bonded IOPADs               |  130 |   130 |       130 | 100.00 |
| PHY_CONTROL                 |    0 |     0 |         4 |   0.00 |
| PHASER_REF                  |    0 |     0 |         4 |   0.00 |
| OUT_FIFO                    |    0 |     0 |        16 |   0.00 |
| IN_FIFO                     |    0 |     0 |        16 |   0.00 |
| IDELAYCTRL                  |    0 |     0 |         4 |   0.00 |
| IBUFDS                      |    0 |     0 |       192 |   0.00 |
| PHASER_OUT/PHASER_OUT_PHY   |    0 |     0 |        16 |   0.00 |
| PHASER_IN/PHASER_IN_PHY     |    0 |     0 |        16 |   0.00 |
| IDELAYE2/IDELAYE2_FINEDELAY |    0 |     0 |       200 |   0.00 |
| ILOGIC                      |    0 |     0 |       200 |   0.00 |
| OLOGIC                      |   20 |    20 |       200 |  10.00 |
|   OUTFF_Register            |   19 |    19 |           |        |
|   OUTFF_ODDR_Register       |    1 |     1 |           |        |
+-----------------------------+------+-------+-----------+--------+


6. Clocking
-----------

+------------+------+-------+-----------+-------+
|  Site Type | Used | Fixed | Available | Util% |
+------------+------+-------+-----------+-------+
| BUFGCTRL   |    7 |     0 |        32 | 21.88 |
| BUFIO      |    0 |     0 |        16 |  0.00 |
| MMCME2_ADV |    2 |     0 |         4 | 50.00 |
| PLLE2_ADV  |    0 |     0 |         4 |  0.00 |
| BUFMRCE    |    0 |     0 |         8 |  0.00 |
| BUFHCE     |    0 |     0 |        72 |  0.00 |
| BUFR       |    0 |     0 |        16 |  0.00 |
+------------+------+-------+-----------+-------+


7. Specific Feature
-------------------

+-------------+------+-------+-----------+-------+
|  Site Type  | Used | Fixed | Available | Util% |
+-------------+------+-------+-----------+-------+
| BSCANE2     |    0 |     0 |         4 |  0.00 |
| CAPTUREE2   |    0 |     0 |         1 |  0.00 |
| DNA_PORT    |    0 |     0 |         1 |  0.00 |
| EFUSE_USR   |    0 |     0 |         1 |  0.00 |
| FRAME_ECCE2 |    0 |     0 |         1 |  0.00 |
| ICAPE2      |    0 |     0 |         2 |  0.00 |
| STARTUPE2   |    0 |     0 |         1 |  0.00 |
| XADC        |    0 |     0 |         1 |  0.00 |
+-------------+------+-------+-----------+-------+


8. Primitives
-------------

+------------+-------+----------------------+
|  Ref Name  |  Used |  Functional Category |
+------------+-------+----------------------+
| FDRE       | 19983 |         Flop & Latch |
| LUT6       |  5102 |                  LUT |
| LUT2       |  4159 |                  LUT |
| LUT3       |  3650 |                  LUT |
| LUT4       |  3004 |                  LUT |
| LUT5       |  2926 |                  LUT |
| SRL16E     |  1361 |   Distributed Memory |
| FDCE       |  1350 |         Flop & Latch |
| CARRY4     |  1180 |           CarryLogic |
| LUT1       |   743 |                  LUT |
| FDSE       |   708 |         Flop & Latch |
| RAMD32     |   284 |   Distributed Memory |
| SRLC32E    |   178 |   Distributed Memory |
| BIBUF      |   130 |                   IO |
| FDPE       |   107 |         Flop & Latch |
| RAMS32     |    86 |   Distributed Memory |
| IBUF       |    60 |                   IO |
| DSP48E1    |    57 |     Block Arithmetic |
| MUXF7      |    56 |                MuxFx |
| OBUFT      |    38 |                   IO |
| OBUF       |    25 |                   IO |
| RAMB18E1   |    17 |         Block Memory |
| RAMB36E1   |    13 |         Block Memory |
| BUFG       |     7 |                Clock |
| MUXF8      |     4 |                MuxFx |
| MMCME2_ADV |     2 |                Clock |
| PS7        |     1 | Specialized Resource |
| ODDR       |     1 |                   IO |
+------------+-------+----------------------+


9. Black Boxes
--------------

+----------+------+
| Ref Name | Used |
+----------+------+


10. Instantiated Netlists
-------------------------

+----------+------+
| Ref Name | Used |
+----------+------+


